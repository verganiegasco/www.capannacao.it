/**
 * @author Luigi
 */
var LV = LV || {};
LV.initialize = function() {
	LV.checkDevice();
	LV.checkPngAlpha();
};

LV.isMobile = false;
LV.pngSupport = 2;
LV.device = "";

LV.FULL_PNG = 2;
LV.MEDIUM_PNG = 1;
LV.NO_PNG = 0;

LV.checkDevice = function(){
		var useragent = navigator.userAgent;
		useragent = useragent.toLowerCase();
	 
		if (useragent.indexOf('iphone') != -1) {
			LV.isMobile = true;
			LV.device = 'iphone';
		} else if(useragent.indexOf('ipad') != -1) {
			LV.isMobile = true;
			LV.device = 'ipad';
		} else if(useragent.indexOf('ipod') !=-1) {
			LV.isMobile = true;
			LV.device = 'ipod';
		} else if(useragent.indexOf('android') != -1) {
			LV.isMobile = true;
			LV.device = 'android';
		} else if(useragent.indexOf('symbianos') != -1 || useragent.indexOf('blackberry') != -1 || useragent.indexOf('samsung') != -1 || useragent.indexOf('nokia') != -1 || useragent.indexOf('windows ce') != -1 || useragent.indexOf('sonyericsson') != -1 || useragent.indexOf('webos') != -1 || useragent.indexOf('wap') != -1 || useragent.indexOf('motor') != -1 || useragent.indexOf('symbian') != -1) {
			LV.isMobile = true;
			LV.device = 'others';
		}
		else 
		{
			LV.isMobile = false;
		}
};

LV.checkPngAlpha = function(){
		var b = $.browser;
		var v = parseInt($.browser.version);
		if(b.msie) {
			if(v>=9) {
				LV.pngSupport = LV.FULL_PNG;
			}
			if(v >= 7 && v<9) {
				LV.pngSupport = LV.MEDIUM_PNG;
			}
			if(v<7) {
				LV.pngSupport = LV.NO;
			}
		}
};

LV.getBrowser = function() {
	return $.browser;
}

LV.getBrowserVersion = function() {
	return $.browser.version;
}

LV.initialize();