/**
 * @author luigi
 */

;
var LV = LV || {};

(function(LV) {
	LV.ImagePreloadAdapter = function(data) {

		var context = this;
		this.src = null;
		this.data = data;
		this._handleStart = function(event) {
			context.triggerEvent('start', null);
		};
		this._handleProgress = function(e) {
			var perc = e.loaded / e.total;
			context.triggerEvent('progress', perc);
		};
		this._handleFileLoad = function(event) {
			context.triggerEvent('fileload', event);
		};

		this._handleComplete = function(event) {
			this.data[0].width = event !== undefined && event.width !== undefined? event.width: this.preloader.width;
			this.data[0].height = event !== undefined && event.height !== undefined? event.height: this.preloader.height;
			var res = event !== undefined ? event: {};
			res.data = this.data;
			context.triggerEvent('complete', res);
		};

		this._handleFileError = function(event) {
			context.triggerEvent('error', event);
		};
		LV.PreloaderAdapter.call(this);
	};
	LV.ImagePreloadAdapter.prototype = new LV.PreloaderAdapter();
	LV.ImagePreloadAdapter.prototype.constructor = LV.ImagePreloadAdapter;
	LV.ImagePreloadAdapter.prototype.initialize = function() {
		/*if(this.isProgress) {
			var xhr = new XMLHttpRequest();
			this.preloader = new XMLHttpRequest();
			this.preloader.onprogress = this._handleProgress;			
		} else {
			
		}*/
		this.preloader = new Image();
		for (var i = 0; i < this.data.length; i++) {
			var str = this.data[i].src;
			this.src = str;
		}

		var self = this;
		this.preloader.onload = function(e) {
			self._handleComplete.call(self, e);
		};
	};

	LV.ImagePreloadAdapter.prototype.load = function() {
		/*if (this.isProgress) {
			this.preloader.open("GET", this.src, true);
			this.preloader.overrideMimeType('text/plain; charset=x-user-defined');
			this.preloader.send();
		} else {
		}*/
		this.preloader.src = this.src;

	};
})(LV);
