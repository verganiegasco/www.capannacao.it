/**
 * @author luigi
 */

;var LV = LV || {};

LV.PreloadJSAdapter = function(data) {
	var context = this;
	this.data = data;
	this._handleStart = function(event) {
		context.triggerEvent('start', null);
	};
	this._handleProgress = function(event) {
		context.triggerEvent('progress', event.progress);
	};
	this._handleFileLoad = function(event) {
		context.triggerEvent('fileload', event);
	};

	this._handleComplete = function(event) {
		context.triggerEvent('complete', event);
	};

	this._handleFileError = function(event) {
		context.triggerEvent('error', event);
	};
	LV.PreloaderAdapter.call(this);
};

LV.PreloadJSAdapter.prototype = new LV.PreloaderAdapter();
LV.PreloadJSAdapter.prototype.constructor = LV.PreloadJSAdapter;
LV.PreloadJSAdapter.prototype.initialize = function() {
	this.preloader = new createjs.LoadQueue();
	this.preloader.addEventListener("fileload", this._handleFileLoad);
	this.preloader.on("progress", this._handleProgress);
	this.preloader.addEventListener("error", this._handleFileError);
	this.preloader.addEventListener("complete", this._handleComplete);
	this.preloader.loadManifest(this.data, false);
	this._handleStart(null);
	this.preloader.setMaxConnections(5);
};

LV.PreloadJSAdapter.prototype.load = function() {
	this.preloader.load();
};