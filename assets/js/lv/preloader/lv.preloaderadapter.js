/**
 * @author luigi
 */

;
var LV = LV || {}; 
(function(LV) {

	LV.PreloaderAdapter = function() {
		this.preloader = null;
		this.initialize();
		this.eventMap = {
			'start' : null,
			'progress' : null,
			'complete' : null,
			'error' : null
		};
		this.isProgress = false;
		this.testProgress();
	};
	LV.PreloaderAdapter.prototype.constructor = LV.PreloaderAdapter;
	LV.PreloaderAdapter.prototype.initialize = function() {

	};
	LV.PreloaderAdapter.prototype.load = function() {

	};

	LV.PreloaderAdapter.prototype.addEventListener = function(event, callback, context) {
		this.eventMap[event] = {
			callback : callback,
			context : context
		};
	};

	LV.PreloaderAdapter.prototype.triggerEvent = function(eventName, data) {
		if (this.eventMap[eventName]) {
			var evtObj = this.eventMap[eventName];
			var callback = evtObj.callback;
			callback.call(evtObj.context, data);
		};
	};
	LV.PreloaderAdapter.prototype.testProgress = function() {
		try {
			var xhr = new XMLHttpRequest();
			if ('onprogress' in xhr) {
				// Browser supports W3C Progress Events
				this.isProgress = true;
			} else {
				// Browser does not support W3C Progress Events
				this.isProgress = false;
			}
		} catch (e) {
			// Browser is IE6 or 7
			this.isProgress = false;
		}
	};

})(LV);

