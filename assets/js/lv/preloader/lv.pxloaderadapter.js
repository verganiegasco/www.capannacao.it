/**
 * @author luigi
 */

;
var LV = LV || {};

(function(LV) {
	LV.PxLoaderAdapter = function(data) {

		var context = this;
		this.data = data;
		this._handleStart = function(event) {
			context.triggerEvent('start', null);
		};
		this._handleProgress = function(event) {	
			var evt = {};		
			evt.perc = event.completedCount / event.totalCount;
			for(var i=0,j=context.data.length; i<j; i++){
			  var item = context.data[i];
			  if(item.src==event.resource.img.src) {
			  	item.width = event.resource.img.width;
			  	item.height = event.resource.img.height;
			  	evt.item = item;
			  }
			  
			};
			
			context.triggerEvent('progress', evt);
		};
		this._handleFileLoad = function(event) {
			context.triggerEvent('fileload', event);
		};

		this._handleComplete = function(event) {

			context.triggerEvent('complete', context.data);
		};

		this._handleFileError = function(event) {
			context.triggerEvent('error', event);
		};
		LV.PreloaderAdapter.call(this);
	};
	LV.PxLoaderAdapter.prototype = new LV.PreloaderAdapter();
	LV.PxLoaderAdapter.prototype.constructor = LV.PxLoaderAdapter;
	LV.PxLoaderAdapter.prototype.initialize = function() {
		this.preloader = new PxLoader();
		for (var i = 0; i < this.data.length; i++) {
			var str = this.data[i].src;
			if (str.indexOf('js', str.length - 2) !== -1) {
				this.preloader.add(new PxLoaderJavascript(str));
			} else {
				this.preloader.add(new PxLoaderImage(str));
			}

		}
		this.preloader.addProgressListener(this._handleProgress);
		this.preloader.addCompletionListener(this._handleComplete);
	};

	LV.PxLoaderAdapter.prototype.load = function() {
		this.preloader.start();
	};
})(LV);
