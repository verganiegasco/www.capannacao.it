var LV = LV || {};


LV.Delayer = function(opts) {
	this.data = opts.array;
	this.animatedObject = opts.animatedObject?opts.animatedObject:{
		cssFrom: {
			opacity: 0
		},
		cssTo: {
			opacity: 1
		},
		time: 1000,
		ease: 'linear'
	};
	this.offset = opts.offset?opts.offset:0;
	this.delay = opts.delay?opts.delay:1000;
	this.total = opts.array.length;
	this.count = 0;
	this.callbackComplete = opts.handleComplete;
	this.callbackStep = opts.handleStep;
	this.handleComplete = function() {
		this.callbackComplete();
	};
	this.handleStep = function(index, element) {
		this.callbackStep(index, element);
	};

};

LV.Delayer.prototype = {
	constructor : VEG.Delayer,
	play: function() {
		var context = this;
		var anObj = this.animatedObject;
		var delay = this.delay;
		var offset = this.offset;
		this.data.each(function(i) { //
			$(this).css(anObj.cssFrom).delay(offset + (i * delay)).animate(anObj.cssTo, anObj.time, anObj.ease, function(){
				context.count++;
				context.handleStep(context.count, this);
				if(context.count >= context.total) {
					context.handleComplete();
				}
			});

		});
	},
	pause:function() {
		
	},
	resume:function() {
		
	}
}