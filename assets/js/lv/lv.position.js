/**
 * @author Luigi Vergani
 */

var LV = LV || {};

LV.Position = function(opts) {
	this.point = {x:0, y:0};
}

LV.Position.TOP_LEFT = "TL";
LV.Position.TOP_CENTER = "TC";
LV.Position.TOP_RIGHT = "TR";

LV.Position.CENTER_LEFT = "CL";
LV.Position.CENTER_CENTER = "CC";
LV.Position.CENTER_RIGHT = "CR";


LV.Position.BOTTOM_LEFT = "BL";
LV.Position.BOTTOM_CENTER = "BC";
LV.Position.BOTTOM_RIGHT = "BR";

LV.Position.HORIZONTAL_CENTER = "HC";
LV.Position.VERTICAL_CENTER = "VC";

LV.Position.prototype = {
	constructor: LV.Position,
	getPosition: function(alignment, contentWidth, contentHeight, containerWidth, containerHeight) {
		switch(alignment) {
			case LV.Position.TOP_LEFT:
				this.point.x = 0;
				this.point.y = 0;
			break;
			case LV.Position.TOP_CENTER:
				this.point.x = (containerWidth-contentWidth)*.5;
				this.point.y = 0;
			break;
			case LV.Position.TOP_RIGHT:
				this.point.x = containerWidth-contentWidth;
				this.point.y = 0;
			break;
			case LV.Position.CENTER_LEFT:
				this.point.x = 0;
				this.point.y = (containerHeight-contentHeight)*.5;
			break;
			case LV.Position.CENTER_CENTER:
				this.point.x = (containerWidth-contentWidth)*.5;
				this.point.y = (containerHeight-contentHeight)*.5;
			break;
			case LV.Position.CENTER_RIGHT:
				this.point.x = containerWidth-contentWidth;
				this.point.y = (containerHeight-contentHeight)*.5;
			break;
			case LV.Position.BOTTOM_LEFT:
				this.point.x = 0;
				this.point.y = (containerHeight-contentHeight);
			break;
			case LV.Position.BOTTOM_CENTER:
				this.point.x = (containerWidth-contentWidth)*.5;
				this.point.y = (containerHeight-contentHeight);
			break;
			case LV.Position.BOTTOM_RIGHT:
				this.point.x = containerWidth-contentWidth;
				this.point.y = (containerHeight-contentHeight);
			break;
			case LV.Position.HORIZONTAL_CENTER:
				this.point.x = (containerWidth-contentWidth)*.5;
				this.point.y = NaN;
			break;
			case LV.Position.VERTICAL_CENTER:
				this.point.x = NaN;
				this.point.y = (containerHeight-contentHeight)*.5;
			break;
		}
		return this.point;
	}
}
