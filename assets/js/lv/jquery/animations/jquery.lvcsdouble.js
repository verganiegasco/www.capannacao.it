(function($) {

	var methods = {
		init : function(opts) {

			var opt = $.extend({}, $.fn.lvCSDouble.defaults, opts);

			return this.each(function() {
				var $el = $(this);
				$el.addClass('lvCSDouble');
				if (!opt.container) {
					opt.container = $el;
				}
				var $active = opt.$active;
				var $prev = opt.$prev;

				$el.data('lvCSDouble', opt);
			});
		},
		animate : function(params, context) {
			return this.each(function() {
				var $el = $(this);
				var opt = $el.data('lvCSDouble');
				var $active = opt.$active;
				var $prev = opt.$prev;
				$active.lvClipSlice({
					container : opt.container,
					nSlices : opt.nSlices,
					type : opt.type
				}).lvClipSlice('createOpened', opt.type);
				$prev.lvClipSlice({
					container : opt.container,
					nSlices : opt.nSlices,
					type : opt.type
				}).lvClipSlice('createOpened', opt.type);

				var aSlices = $active.lvClipSlice('getSlices');
				var pSlices = $prev.lvClipSlice('getSlices');

				for (var i = 0, j = aSlices.length; i < j; i++) {
					var $s = aSlices[i];
					switch(opt.type) {
						case 'H':
							var left = (i % 2 == 0) ? $s.width() : -$s.width();
							left = left*opt.direction;
							$s.css('left', left);
							break;
						case 'V':
							var top = (i % 2 == 0) ? $s.height() : -$s.height();
							top = top*opt.direction;
							$s.css('top', top);
							break;
					}

				}

				var time = opt.animationTime;
				var ease = opt.ease;

				for (var i = 0, j = aSlices.length; i < j; i++) {
					var $s = aSlices[i];
					TweenMax.to($s, time, {
						left : 0,
						top : 0,
						ease : ease
					});
					if (i == opt.nSlices - 1) {
						TweenMax.to($s, time, {
							left : 0,
							top : 0,
							ease : ease,
							onComplete : function() {
								if (params !== undefined) {
									params.onComplete.call(context, params.onCompleteParams);
								}
							}
						});
					}
				}
				for (var i = 0, j = pSlices.length; i < j; i++) {
					var $s = pSlices[i];
					switch(opt.type) {
						case 'H':
							var left = (i % 2 == 0) ? -$s.width() : $s.width();
							left = left*opt.direction;
							TweenMax.to($s, time, {
								left : left,
								ease : ease
							});
							break;
						case 'V':
							var top = (i % 2 == 0) ? -$s.height() : $s.height();
							top = top*opt.direction;
							TweenMax.to($s, time, {
								top : top,
								ease : ease
							});
							break;
					}

				}
			});
		},
		destroy : function() {
			return this.each(function() {
				var $t = $(this);
				var opt = $t.data('lvCSDouble');
				opt.$active.lvClipSlice('destroy');
				opt.$prev.lvClipSlice('destroy');
			});
		}
	};

	$.fn.lvCSDouble = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if ( typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.position');
		}

	};
	$.fn.lvCSDouble.defaults = {
		$active : null,
		$prev : null,
		container : null,
		animationTime : 1,
		nSlices : 2,
		ease : Circ.easeInOut,
		type : 'V',
		direction: 1
	};
})(jQuery);
