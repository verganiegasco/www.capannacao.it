(function($) {
		
	var methods = {
		init : function(opts) {
			
			var options = $.extend({}, $.fn.lvLoadImg.defaults, opts);
			
			return this.each(function() {
				$(this).data('options', options);
				$(this).data('isLoaded', false);
				if(options.autoload==true) {
					$(this).lvLoadImg('load');
				}
			});

		},
		destroy :function() {
		},
		load: function(completeHandler) {
			return this.each(function() {
				if(!$(this).data('isLoaded')) {
					var options = $(this).data('options');
					var $img = $(this);
					var $source = $img.attr('data-source');
					if(options.animation!=null) {
						$img.css({
							opacity: 0
						});
					}
					$img.attr('src', $source);
					$img.load(function(){
						if(options.animation!=null) {
							$img.css({
								opacity: 0
							}).delay(options.animation.delay).animate({
								opacity: 1
							}, options.animation.time);
						}
						
						$img.data('isLoaded', true);
						if(completeHandler) {
							completeHandler();
						}
						options.handleLoaded();
					});
				}
								
			});
		}
	};


	$.fn.lvLoadImg = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(
					arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.position');
		}

	};
	$.fn.lvLoadImg.defaults = {
			handleLoaded: function() {

			},
			handleOpen:function() {
				
			},
			autoload: true,
			animation: {
				delay: 0,
				time: 500
			}
	};
})(jQuery);