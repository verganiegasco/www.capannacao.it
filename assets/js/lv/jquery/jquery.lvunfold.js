;(function($) {

	var counter = 0;

	function createTransition($t) {

		$t.children('div.th-inner.first').wrap($('<div class="th-part first"></div>')).parent().append('<div class="th-overlay"></div>').clone().addClass('clone').appendTo($t);
		$t.children('div.th-inner.second').wrap($('<div class="th-part second"></div>')).parent().append('<div class="th-overlay"></div>').clone().addClass('clone').insertAfter($t.find('.th-part.second'));

	}
	function destroyTransition($t) {

		$t.find('.th-part.first.clone, div.th-part.second.clone').remove();
		$t.find('.th-inner.first, div.th-inner.second').unwrap();
		$t.find('.th-overlay').remove();

	}

	var methods = {
		init : function(opts) {
			return this.each(function() {
				var opt = $.extend({}, $.fn.lvUnfold.defaults, opts);
				var $t = $(this);
				createTransition($t);
				if(opt.auto) {
					$t.click(function() {
						if (!$(this).hasClass('open')) {
							$(this).addClass('open');
							$t.lvUnfold('over');
	
						} else {
							$(this).removeClass('open');
							$t.lvUnfold('out');
						}
					});
				}

				$t.data('lvUnfold', opt);
			});
		},
		out : function(params) {
			return this.each(function() {
				var $t = $(this);
				var opt = $t.data('lvUnfold');
				$t.find('.th-overlay').css({
					opacity : 0
				});
				$t.data('lvUnfold', opt);
				
				/**
				 * SETTING INIZIALI 
				 */
				
				TweenMax.to($t.find('.th-part.first').get(0), 0, {
					top : "-100%",
					ease : opt.ease
				});
				TweenMax.to($t.find('.th-part.first').get(1), 0, {
					bottom : "-100%",
					ease : opt.ease
				});
				
				/**
				 * ANIMA
				 */
				TweenLite.to($t.find('.th-part.second').get(0), 0, {
					transformOrigin : "center bottom"
				});
				TweenMax.to($t.find('.th-overlay'), opt.animationTime, {
					opacity : 1
				});
				TweenMax.to($t.find('.th-part.second').get(0), opt.animationTime, {
					rotationX: -90,
					z: -$($t.find('.th-part').get(2)).height()*.5,
					ease : opt.ease
				});
				TweenMax.to($t.find('.th-part.second').get(1), opt.animationTime, {
					rotationX: 90,
					z: -$($t.find('.th-part').get(2)).height()*.5,
					ease : opt.ease
				});
				TweenMax.to($t.find('.th-part.first').get(0), opt.animationTime, {
					top : "0",
					ease : opt.ease
				});
				TweenMax.to($t.find('.th-part.first').get(1), opt.animationTime, {
					bottom : "0",
					ease : opt.ease,
					onComplete : function() {
						if (params !== undefined) {
							params.onComplete.call(params.context, params.onCompleteParams);
						}
					}
				});

			});
		},
		over : function(params) {
			return this.each(function() {
				var $t = $(this);
				var opt = $t.data('lvUnfold');
				$t.data('lvUnfold', opt);
				$t.find('.th-overlay').css({
					opacity : 1
				});
				/**
				 * SETTING INIZIALI 
				 */
				TweenMax.to($t.find('.th-part').get(1), 0, {
					rotationX: -90,
					z: -$($t.find('.th-part').get(1)).height()*.5
				});
				TweenMax.to($t.find('.th-part').get(2), 0, {
					rotationX: 90,
					z: -$($t.find('.th-part').get(2)).height()*.5
				});
				/**
				 * ANIMA
				 */
				TweenMax.to($t.find('.th-overlay'), opt.animationTime, {
					opacity : 0,
					delay: .5
				});
				TweenMax.to($t.find('.th-part.second'), opt.animationTime, {
					transform : "rotateX(0deg) translateZ(0px)",
					ease : opt.ease
				});
				TweenMax.to($t.find('.th-part.first').get(0), opt.animationTime, {
					top : "-100%",
					ease : opt.ease
				});
				TweenMax.to($t.find('.th-part.first').get(1), opt.animationTime, {
					bottom : "-100%",
					ease : opt.ease,
					onComplete : function() {
						if (params !== undefined) {
							params.onComplete.call(params.context, params.onCompleteParams);
						}
					}
				});
			});
		},
		destroy : function() {
			return this.each(function() {
				var $t = $(this);
				var opt = $t.data('lvUnfold');
				destroyTransition($t);
				$t.data('lvUnfold', null);
			});
		}
	};

	$.fn.lvUnfold = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if ( typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.position');
		}

	};
	$.fn.lvUnfold.defaults = {
		animationTime : .5,
		selectedClass : 'active',
		auto : true,
		delay : .2,
		ease : Circ.easeInOut,
		type : 'V'
	};
})(jQuery);
