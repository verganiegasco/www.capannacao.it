/**
 *ABBOZZATO DA COMPLETARE 
 */


(function($) {
		
	var methods = {
		init : function(opts) {
			
			var options = $.extend({}, $.fn.lvBgInfiniteScroll.defaults, opts);
			var context = this;
			
			return this.each(function() {
				var $this = $(this);
				$(this).data('optionsLvBgInfiniteScroll', options);
				
				for (var i = 0; i < options.imgRepeat; i++){
				  var $img = $($this.find('img').get(0));
				  var clone = $img.clone();
				  $this.append(clone);
				};
				/*$(this).css({
					overflow: 'hidden'
				});*/
				$(this).find('img').css({
					display: 'block',
					position: 'absolute'
				});
				if(options.autoload==true) {
					$(this).lvBgInfiniteScroll('play');
				}
				$this.lvBgInfiniteScroll('resize');
				$this.lvBgInfiniteScroll('animate');
			});

		},
		destroy :function() {
		},
		resize: function() {
			return this.each(function() {
				var options = $(this).data('optionsLvBgInfiniteScroll');
				var $this = $(this);
				var $img = $($this.find('img').get(0));
				$this.width($img.width());
				$this.height($img.height());
			});
		},
		play: function(completeHandler) {
			return this.each(function() {
				var options = $(this).data('optionsLvBgInfiniteScroll');

								
			});
		},
		animate: function() {
			return this.each(function() {
				/*var options = $(this).data('optionsLvBgInfiniteScroll');
				var duration = options.tickerTimer;
				var $this = $(this);
				//$this.data('animation', setTimeout(animate, duration, $this));
				$this.data('animation', setInterval(animate, duration, $this));
				//setTimeout(animate, duration)
				
				
				function animate($parent) {
					$img = $($parent.find('img').get(0));
					var pos = parseInt($img.css('left'));
					//console.log($img);
					if(pos>-$img.width()) {
						$img.css({
							'left': pos-1
						});
					} else {
						$img.css({
							'left': 0
						});
					}
					

					$parent.find('img').each(function(index){
						var $this = $(this);
						if(index > 0) {
							$prev = $($parent.find('img').get(index-1));
							var prevX = parseInt($prev.css('left'));
							$this.css({
								left: prevX+$prev.width(),
								top: 0
							});
							//console.log($this.css('left'));
						}
					});
					//$parent.data('animation', setTimeout(animate, duration, $this));
				}*/
								
			});
		}
	};


	$.fn.lvBgInfiniteScroll = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(
					arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.position');
		}

	};
	$.fn.lvBgInfiniteScroll.defaults = {
			handlePaused: function() {

			},
			handleResume:function() {
				
			},
			handleStart:function() {
				
			},
			autoplay: true,
			tickerTimer: 50,
			imgRepeat: 1
	};
})(jQuery);