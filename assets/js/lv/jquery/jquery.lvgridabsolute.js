(function($) {

	var methods = {
		init : function(opts) {

			var opt = $.extend({}, $.fn.lvGridAbsolute.defaults, opts);

			return this.each(function() {
				var $el = $(this);
				$el.data('lvGridAbsolute', opt);
				if (opt.auto == true) {
					$el.lvGridAbsolute('render');
					$(window).bind("resize.lvGridAbsolute", function() {
						$el.lvGridAbsolute('render');
					});
				}
			});
		},
		updateMinMargin : function(margin) {
			return this.each(function() {
				var opt = $(this).data('lvGridAbsolute');
				if (!opt)
					return false;
				opt.minMargin = margin;
				var $t = $(this);
				$t.data('lvGridAbsolute', opt);
			});
		},
		render : function() {
			return this.each(function() {
				var opt = $(this).data('lvGridAbsolute');
				if (!opt)
					return false;
				var $el = $(this);
				var w = $el.lvCss('width');
				var itemW = $el.find(opt.itemClass).lvCss('width') + opt.minMargin;
				var itemsNum = Math.floor((w + opt.minMargin) / itemW);
				var itemsWidth = (itemsNum * itemW) - opt.minMargin;
				var itemMargin = Math.floor((w - ($el.find(opt.itemClass).lvCss('width') * itemsNum)) / (itemsNum - 1));
				if (itemMargin < opt.minMargin) {
					itemMargin = opt.minMargin;
				}
				var $prev = null;
				var i = 0;
				$el.find(opt.itemClass).each(function(index) {
					var $t = $(this);
					var p = {
						x : 0,
						y : 0
					};
					if (index > 0) {
						if(i==0) {
							p.x = 0;
							p.y = (opt.marginTopAuto)?$prev.lvCss('top') + $prev.lvCss('height')+itemMargin:$prev.lvCss('top') + $prev.lvCss('height');
						} else {
							p.x = $prev.lvCss('left') + $prev.lvCss('width') + itemMargin;
							p.y = $prev.lvCss('top');
						}

					}
					$t.css({
						top : p.y,
						left : p.x
					});
					$t.attr('data-x', p.x);
					$t.attr('data-y', p.y);
					$t.attr('data-width', $t.width());
					$t.attr('data-height', $t.height());
					$prev = $t;
					if (i < itemsNum - 1) {
						i++;
					} else {
						i = 0;
					}
				});
			});
		},
		destroy : function() {
			$(window).unbind("resize.lvGridAbsolute");
		}
	};

	$.fn.lvGridAbsolute = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if ( typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.lvGridAbsolute');
		}

	};
	$.fn.lvGridAbsolute.defaults = {
		itemClass : '.item',
		minMargin : 0,
		auto : true,
		marginTop: 0,
		marginTopAuto: true
	};
})(jQuery);
