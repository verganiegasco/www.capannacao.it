(function($) {
	

	var methods = {
		init : function(opts) {

			var opt = $.extend({}, $.fn.lv3DRotator.defaults, opts);

			return this.each(function() {
				var $t = $(this);
				var anim = null;
				var $back = $($t.find(opt.backClass));
				var $front = $($t.find(opt.frontClass));
				
				$back.css({
					'backface-visibility': 'hidden',
					'-webkit-backface-visibility': 'hidden'
				});
				$front.css({
					'backface-visibility': 'hidden',
					'-webkit-backface-visibility': 'hidden'
				});
				
				TweenMax.to($back, 0, {
					rotationY: 180,
					immediateRender: true
				});
				
				$t.hover(function() {
					if(!$t.hasClass('active')) {
						$t.lv3DRotator('over');
					}
				}, function() {
					if(!$t.hasClass('active')) {
						$t.lv3DRotator('out');
					}
				});
				$t.data('lv3DRotator', opt);
			});
		},
		out: function() {
			return this.each(function() {
				var $t = $(this);
				var opt = $t.data('lv3DRotator');
				var $back = $($t.find(opt.backClass));
				var $front = $($t.find(opt.frontClass));
				TweenMax.to($back, opt.animationTime, {
					rotationY: 180,
					ease: opt.ease
				});
				TweenMax.to($front, opt.animationTime, {
					rotationY: 0,
					ease: opt.ease
				});
				$t.data('lv3DRotator', opt);
			});
		},
		over: function() {
			return this.each(function() {
				var $t = $(this);
				var opt = $t.data('lv3DRotator');
				var $back = $($t.find(opt.backClass));
				var $front = $($t.find(opt.frontClass));
				TweenMax.to($back, opt.animationTime, {
					rotationY: 0,
					ease: opt.ease
				});
				TweenMax.to($front, opt.animationTime, {
					rotationY: 180,
					ease: opt.ease
				});
				$t.data('lv3DRotator', opt);
			});
		},
		destroy : function() {
		}
	};

	$.fn.lv3DRotator = function(method) {
		if(methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if( typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.position');
		}

	};
	$.fn.lv3DRotator.defaults = {
		animationTime: 1,
		frontClass: '.front',
		backClass: '.back',
		ease: Circ.easeInOut,
		auto: true,
		direction: 1
	};
})(jQuery);
