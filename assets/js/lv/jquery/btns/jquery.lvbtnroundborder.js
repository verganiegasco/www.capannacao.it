(function($) {
	
	function getClipStr(T,R,B,L) {
		var rect =  'rect(' + T + 'px ' + R + 'px ' + B + 'px ' + L + 'px)';
		return rect;
	}

	function setClip($el, T, R, B, L) {
		$el.css({
			clip : getClipStr(T, R, B, L)
		});
	}

	var methods = {
		init : function(opts) {

			var opt = $.extend({}, $.fn.lvBtnRoundBorder.defaults, opts);

			return this.each(function() {
				var $t = $(this);
				var anim = null;
				if(!opt.outColor) {
					opt.outColor = $t.css('color');
				}
				
				if(opt.removeBorder) {
					$t.css({
						border: 'none'
					})
				}
				
				$t.wrapInner('<div class="lv-btn-round-border"></div>');
				$inner = $('<div class="inner"></div>');
				$innerBefore = $('<div class="inner-before"></div>');
				$el1 = $('<div class="slice"><div class="pie"></div></div>');
				$el2 = $el1.clone();
				$el1.addClass('slice-sx');
				$el2.addClass('slice-dx');
				
				$inner.append($el1);
				$inner.append($el2);
				$t.find('.lv-btn-round-border').prepend($inner);
				$t.find('.lv-btn-round-border').prepend($innerBefore);
				$t.lvBtnRoundBorder('render');
				$t.find('.pie').css({
					border: opt.stroke+'px solid '+opt.overColor
				});
				$innerBefore.css({
					border: opt.stroke+'px solid '+opt.outColor
				});
				
				$t.hover(function() {
					if(!$t.hasClass('active')) {
						$t.lvBtnRoundBorder('over');
					}
				}, function() {
					var spans = $t.find('span').toArray();
					if(!$t.hasClass('active')) {
						$t.lvBtnRoundBorder('out');
					}
				});
				$t.data('lvBtnRoundBorder', opt);
			});
		},
		render: function() {
			return this.each(function() {
				var $t = $(this);
				var opt = $t.data('lvBtnRoundBorder');
				var $el1 = $($t.find('.slice-sx'));
				var $el2 = $($t.find('.slice-dx'));
				setClip($el1, 0, $t.width()*.5, $t.height(),0);
				setClip($el1.find('.pie'), 0, $t.width()*.5, $t.height(),0);
				setClip($el2, 0, $t.width(), $t.height(),$t.width()*.5);
				setClip($el2.find('.pie'), 0, $t.width()*.5, $t.height(),0);
			});
		},
		out: function() {
			return this.each(function() {
				var $t = $(this);
				var opt = $t.data('lvBtnRoundBorder');
				TweenMax.to($t.find('.slice-dx .pie'), opt.animationTime, {
					css: {
						rotation: 0
					},
					ease: opt.ease
				});
				TweenMax.to($t.find('.slice'), opt.animationTime, {
					css: {
						rotation: 0
					},
					ease: opt.ease
				});
				TweenMax.to($t.find('.slice-sx .pie'), opt.animationTime, {
					css: {
						rotation: 180
					},
					ease: opt.ease
				});
				$t.data('lvBtnRoundBorder', opt);
			});
		},
		over: function() {
			return this.each(function() {
				var $t = $(this);
				var opt = $t.data('lvBtnRoundBorder');
				TweenMax.to($t.find('.slice-dx .pie'), opt.animationTime, {
					css: {
						rotation: 180
					},
					ease: opt.ease
				});
				TweenMax.to($t.find('.slice'), opt.animationTime, {
					css: {
						rotation: 180
					},
					ease: opt.ease
				});
				TweenMax.to($t.find('.slice-sx .pie'), opt.animationTime, {
					css: {
						rotation: 360
					},
					ease: opt.ease
				});
				$t.data('lvBtnRoundBorder', opt);
			});
		},
		destroy : function() {
		}
	};

	$.fn.lvBtnRoundBorder = function(method) {
		if(methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if( typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.position');
		}

	};
	$.fn.lvBtnRoundBorder.defaults = {
		outColor: "#CCCCCC",
		overColor: "#000000",
		animationTime: 1,
		selectedClass: 'active',
		ease: Circ.easeInOut,
		auto: true,
		stroke: 5,
		removeBorder: false
	};
})(jQuery);
