/**
 * 2012-04-14: introdotta dipendenza da LV.Position;
 */

;(function($) {


	var methods = {
		init : function(opts) {
			var options = $.extend({}, $.fn.lvPosition.defaults, opts);
			return this.each(function() {
				
				var $el = $(this);
				
				
				
				if($el.attr('data-position') && $el.attr('data-position')!="") {
					options.position = $el.attr('data-position');
				}
								
				
				$el.data('lvPosition', options);
				$el.lvPosition('position');
				if(options.auto == true) {
					$(window).bind('resize.lvPosition',function(){
						$el.lvPosition('position');
					});
				}
			});

		},
		position : function(w, h) {
			return this.each(function() {
				var opt = $(this).data('lvPosition');
				if(!opt) return false;
				opt.handleStartReposition();
				var $el = $(this);
				var position = new LV.Position();
				var container = $(window);
				if(opt.container!=null) {
					container = opt.container;
				}
				if(opt.useParent) {
					container = $el.parent();
				}
				
				var containerWidth = container.width();
				var containerHeight = container.height();
				
				if(w && h) {
					containerWidth = w;
					containerHeight = h;
				}
				
				var point = position.getPosition(opt.position,parseInt($el.css('width')), parseInt($el.css('height')), containerWidth, containerHeight);
				var x = !isNaN(point.x)?point.x:parseInt($el.css('left'));
				var y = !isNaN(point.y)?point.y:parseInt($el.css('top'));
				
				$el.css({
					top: y+opt.offsetY,
					left: x+opt.offsetX 
				});
				$el.data('lvPositionPoint', point);
				opt.handleReposition($el.css('left'), $el.css('top'));
			});
		},
		destroy :function() {
			$(window).unbind('resize.lvPosition');
		},
		get: function() {
			//return this.each(function() {
				var point = $(this).data('lvPositionPoint');
				return point;
			//});
		},
		updateOption: function(key, value) {
			return this.each(function() {
				var $t = $(this);
	            var opt = $t.data('lvPosition');
	            if (value!==null && opt != undefined) {
	                opt[key] = value;
	                $t.data('lvPosition', opt);
	            } else {
	            	
	            }
            });
		}
	};


	$.fn.lvPosition = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(
					arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.position');
		}

	};
	$.fn.lvPosition.defaults = {
			auto: true,
			offsetY: 0,
			offsetX: 0,
			position : 'CL',
			container: null,
			handleReposition : function(nx, ny) {
				return {
					x : nx,
					y : ny
				};
			},
			handleStartReposition: function() {
				
			},
			useParent: false
	};
})(jQuery);