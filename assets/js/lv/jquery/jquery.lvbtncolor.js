(function($) {

	var methods = {
		init : function(opts) {

			return this.each(function() {
				var opt = $.extend({}, $.fn.lvBtnColor.defaults, opts);
				var $el = $(this);
				$el.lettering();
				var anim = null;
				if (!opt.outColor) {
					opt.outColor = $el.css('color');
				}
				if(opt.auto) {
					$el.hover(function() {
						if (!$el.hasClass('active')) {
							$el.lvBtnColor('over');
						}
					}, function() {
						var spans = $el.find('span').toArray();
						if (!$el.hasClass('active')) {
							$el.lvBtnColor('out');
						}
					});
				}
				$el.data('lvBtnColor', opt);
			});
		},
		out : function() {
			return this.each(function() {
				var $el = $(this);
				var opt = $el.data('lvBtnColor');
				var spans = $el.find('span').toArray();
				switch(opt.directionOver) {
					case 'DX':
						spans.reverse();
						break;
				}
				opt.anim = TweenMax.staggerTo(spans, opt.animationTime, {
					color : opt.outColor
				}, opt.interval);
				$el.data('lvBtnColor', opt);
			});
		},
		over : function() {
			return this.each(function() {
				var $el = $(this);
				var opt = $el.data('lvBtnColor');
				var spans = $el.find('span').toArray();
				switch(opt.directionOver) {
					case 'DX':
						spans.reverse();
						break;
				}
				opt.anim = TweenMax.staggerTo(spans, opt.animationTime, {
					color : opt.overColor
				}, opt.interval);
				$el.data('lvBtnColor', opt);
			});
		},
		destroy : function() {
		}
	};

	$.fn.lvBtnColor = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if ( typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.position');
		}

	};
	$.fn.lvBtnColor.defaults = {
		outColor : null,
		overColor : "#FFFFFF",
		animationTime : .5,
		interval : .05,
		directionOver : 'SX',
		directionOut : 'DX',
		selectedClass : 'active',
		auto : true
	};
})(jQuery);
