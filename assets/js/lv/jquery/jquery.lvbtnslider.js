(function($) {

	var methods = {
		init : function(opts) {

			var options = $.extend({}, $.fn.lvBtnSlider.defaults, opts);

			return this.each(function() {
				var $originalElement = $(this);
				var $wrapper = $('<div class="lvBtnSlider" />');
				$originalElement.wrap($wrapper);
				var $parent = $originalElement.parent();
				$originalElement.clone().addClass('cloned').appendTo($parent);

				$parent.find('a').css({
					display : 'block',
					position : 'absolute'
				});
				$parent.css({
					height : $originalElement.height()+ parseInt($originalElement.css('padding-top'))+ parseInt($originalElement.css('padding-bottom')),
					width : $originalElement.width() + parseInt($originalElement.css('padding-left'))+ parseInt($originalElement.css('padding-right')),
					overflow : 'hidden',
					position : 'relative'
				});
				$parent.find('.cloned').css({
					top : $parent.height(),
					left : 0
				});
				$parent.hover(function(e) {
					if(!$(this).hasClass('selected')) {
						$originalElement.stop().animate({
							'top' : -$originalElement.height()
						}, 100);
						$parent.find('.cloned').stop().animate({
							'top' : 0
						}, 100);
					}
				}, function(e) {
					if(!$(this).hasClass('selected')) {
						$originalElement.stop().animate({
							'top' : '0px'
						}, 100);
						$parent.find('.cloned').stop().animate({
							'top' : $parent.height()
						}, 100);
					}
				});
				$parent.bind('click.lvBtnSlider', function(e) {
					$parent.addClass('selected');
				});
			});
		},
		reset : function() {
			return this.each(function() {
				var $originalElement = $(this);
				var $parent = $originalElement.parent();
				$parent.removeClass('selected');
				$originalElement.stop().animate({
					'left' : '0px'
				}, 100);
				$parent.find('.cloned').stop().animate({
					'left' : $parent.width()
				}, 100);
			});
		},
		destroy : function() {
		}
	};

	$.fn.lvBtnSlider = function(method) {
		if(methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if( typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.position');
		}

	};
	$.fn.lvBtnSlider.defaults = {

	};
})(jQuery);
