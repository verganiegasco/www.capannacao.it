(function($) {

	var methods = {
		init : function(opts) {

			var opt = $.extend({}, $.fn.lvGrid.defaults, opts);

			return this.each(function() {
				var $el = $(this);
				$el.data('lvGrid', opt);
				if(opt.auto==true) {
					$el.lvGrid('render');
					$(window).bind("resize.lvGrid", function() {
						$el.lvGrid('render');
					});
				}
			});
		},
		updateMinMargin: function(margin) {
			return this.each(function() {
				var opt = $(this).data('lvGrid');
				if(!opt) return false;
				opt.minMargin = margin;
				var $t = $(this);
				$t.data('lvGrid', opt);
			});
		},
		render : function() {
			return this.each(function() {
				var opt = $(this).data('lvGrid');
				if(!opt) return false;
				var $el = $(this);
				var w = $el.lvCss('width');
				var itemW = $el.find(opt.itemClass).lvCss('width')+opt.minMargin;
				var itemsNum = Math.floor((w+opt.minMargin)/itemW);
				var itemsWidth = (itemsNum * itemW)-opt.minMargin;
				var itemMargin = Math.floor((w - ($el.find(opt.itemClass).lvCss('width')*itemsNum)) / (itemsNum-1));
				if(itemMargin<opt.minMargin) {
					itemMargin = opt.minMargin;
				}		
				var $prev = null;
				var i = 0;
				$el.find(opt.itemClass).each(function(index) {
					var $t = $(this);
					if(i==itemsNum-1) {
						$t.css(opt.marginProperty,0);
					} else {
						$t.css(opt.marginProperty,itemMargin);
					}
					$prev = $t;
					if(i<itemsNum-1) {
						i++;
					} else {
						i=0;
					}
				});
			});
		},
		destroy : function() {
			$(window).unbind("resize.lvGrid");
		}
	};

	$.fn.lvGrid = function(method) {
		if(methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if( typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.position');
		}

	};
	$.fn.lvGrid.defaults = {
		itemClass: '.item',
		minMargin: 10,
		auto: true,
		marginProperty: 'margin-right'
	};
})(jQuery);
