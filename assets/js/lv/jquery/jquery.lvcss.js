/**
 * 2012-04-14: introdotta dipendenza da LV.Size;
 */

;(function($) {

	var PIXEL = "px";
	var PERC = "%";

	function getInt(value) {
		return parseInt(value);
	}

	function getFormatVal(value) {
		if ((new RegExp('%')).test(value)) {
			return PERC;
		}
		if ((new RegExp('px')).test(value)) {
			return PIXEL;
		}
	}

	function percToNumber(perc, numb) {
		return numb * perc / 100;
	}

	var methods = {
		backgroundPositionX : function(data) {
			var $this = this;
			if (data) {
				var posY = $this.lvCss('backgroundPositionY');
			}
			var backgroundPos = $this.css('backgroundPosition').split(" ");
			var pos = backgroundPos[0];
			var type = getFormatVal(pos);
			var res = pos;
			switch (type) {
				case PERC:
					res = percToNumber(getInt(pos), $this.width());
					break;
				case PIXEL:
					res = getInt(pos);
					break;
			}

			return res;

		},
		backgroundPositionY : function(opts) {
			var $this = this;
			var backgroundPos = $(this).css('backgroundPosition').split(" ");
			var pos = getInt(backgroundPos[1]);
			return pos;

		},
		offsetTop : function() {
			var $this = this;
			if($this.offset() != undefined && $this.offset()) {
				return $this.offset().top;
			} else {
				return NaN;
			}
		},
		bottom : function() {
			var $this = this;
			var r = $this.css('bottom').replace('px', '');
			return parseInt(r);
		},
		top : function() {
			var $this = this;
			return $this.position().top;
		},
		left : function() {
			var $this = this;
			var left = $this.css('left').replace('px', '');
			return parseInt(left);
		},
		translateX : function() {
			var $this = this;
			var matrix = $this.css('transform').replace(/[^0-9\-.,]/g, '').split(',');
			var x = matrix[12] || matrix[4];
			//var y = matrix[13] || matrix[5];
			return x;
		},
		marginTop : function() {
			var $this = this;
			var m = $this.css('margin-top').replace('px', '');
			return parseInt(m);
		},
		width : function() {
			var $this = this;
			var m = $this.css('width');
			return parseFloat(m);
		},
		height : function() {
			var $this = this;
			var m = $this.css('height');
			return parseFloat(m);
		},
		paddingLeft : function() {
			var $this = this;
			var m = $this.css('padding-left').replace('px', '');
			return parseInt(m);
		}
	};

	$.fn.lvCss = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if ( typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.position');
		}

	};
})(jQuery);
