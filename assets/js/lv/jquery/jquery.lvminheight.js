(function($) {

	var methods = {
		init : function(opts) {

			var opt = $.extend({}, $.fn.lvMinHeight.defaults, opts);

			return this.each(function() {
				var $el = $(this);
				$el.data('lvMinHeight', opt);
				if(opt.auto==true) {
					$el.lvMinHeight('render');
					$(window).bind("resize.lvMinHeight", function() {
						$el.lvMinHeight('render');
					});
				}
			});
		},
		render : function() {
			return this.each(function() {
				var opt = $(this).data('lvMinHeight');
				if(!opt) return false;
				var $el = $(this);
				var stack = [];
				var max = 0;
				var $p = null;
				var columns = Math.floor($el.width() / $el.find(opt.itemClass).width());
				$el.find(opt.itemClass).css('height', "");
				$el.find(opt.itemClass).each(function(index) {
					var $t = $(this);
					stack.push($t);
					max = $t.height() > max?$t.outerHeight():max;
					$.each(stack, function(index) {
					  	$(this).css('height', max);
					});
					if((index+1)%columns==0) {
						stack = [];
						max = 0;
					}
					
					
				});
			});
		},
		destroy : function() {
		}
	};

	$.fn.lvMinHeight = function(method) {
		if(methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if( typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.position');
		}

	};
	$.fn.lvMinHeight.defaults = {
		itemClass: '.item',
		auto: true
	};
})(jQuery);
