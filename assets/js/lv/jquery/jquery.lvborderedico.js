;(function($) {

	var methods = {
		init : function(opts) {

			var options = $.extend({}, $.fn.lvBorderedIco.defaults, opts);
			return this.each(function() {
				
				var $this = $(this);
				var $img = $this.find('img');
				var w = $img.width();
				var h = $img.height();
				var offset = options.offset;
				$this.css({
					'background-color' : options.backgroundColor,
					overflow : 'hidden',
					display : 'block'
				});
				$this.width(w);
				$this.height(h);
				$img.wrap('<div class="wrapper"></div>');
				var $wrapper = $this.find('.wrapper');
				$wrapper.css({
					display : 'block',
					position : 'absolute'
				});
				$wrapper.width(w);
				$wrapper.height(h);
				$this.data('options', options);
				TweenMax.to($wrapper, 0, {
					clip : 'rect(0px ' + w + 'px ' + h + 'px 0px)',
					immediateRender : true
				});
				$this.hover(function() {
					TweenMax.to($wrapper, .3, {
						clip : 'rect(' + offset + 'px ' + (w - offset) + 'px ' + (h - offset) + 'px ' + offset + 'px)',
						ease : Circ.easeInOut,
						onComplete : function() {
						}
					});
					if (options.scale) {
						var conf = options.scaleConfig;
						TweenMax.to($img, conf.over.time, {
							scale : conf.over.scale,
							ease : conf.over.ease,
							onComplete : function() {
							}
						});
					}

				}, function() {
					TweenMax.to($wrapper, .3, {
						clip : 'rect(0px ' + w + 'px ' + h + 'px 0px)',
						ease : Circ.easeInOut,
						onComplete : function() {
						}
					});
					if (options.scale) {
						TweenMax.to($img, .3, {
							scale : 1,
							ease : Circ.easeInOut,
							onComplete : function() {
							}
						});
					}
				});
			});

		},
		destroy : function() {
		},
		resize : function() {
			return this.each(function() {
				var options = $(this).data('options');
			});
		},
		load : function() {
			return this.each(function() {
			});
		}
	};

	$.fn.lvBorderedIco = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if ( typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.lvBorderedIco');
		}

	};
	$.fn.lvBorderedIco.defaults = {
		backgroundColor : '#FF0000',
		offset : 5,
		scale : true,
		scaleConfig : {
			over : {
				time : .3,
				scale : 1.1,
				ease : Circ.easeInOut
			},
			out : {
				time : .3,
				scale : 1,
				ease : Circ.easeInOut
			}
		}
	};
})(jQuery);
