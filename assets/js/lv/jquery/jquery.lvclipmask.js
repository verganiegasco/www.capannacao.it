(function($) {
	
	function getClipStr(T,R,B,L) {
		var rect =  'rect(' + T + 'px ' + R + 'px ' + B + 'px ' + L + 'px)';
		return rect;
	}

	function setClip($el, T, R, B, L) {
		$el.css({
			clip : getClipStr(T, R, B, L)
		});
	}

	function removeMask($el) {
		$el.css({
			clip : 'auto'
		});
	}

	var methods = {
		init : function(opts) {

			var opt = $.extend({}, $.fn.lvClipMask.defaults, opts);

			return this.each(function() {
				var $el = $(this);
				if (!opt.container) {
					opt.container = $el;
				}
				setClip($el, opt.container.height(), opt.container.width(), opt.container.height(), 0);
				$el.data('lvClipMask', opt);
			});
		},
		open : function(params, context) {
			return this.each(function() {
				var $el = $(this);
				var opt = $el.data('lvClipMask');
				setClip($el, opt.container.height(), opt.container.width(), opt.container.height(), 0);
				TweenMax.to($el, opt.animationTime, {
					clip: getClipStr(0, opt.container.width(), opt.container.height(), 0),
					ease: opt.ease,
					onComplete : function() {
						if (params !== undefined) {
							params.onComplete.call(context, params.onCompleteParams);
						}
						removeMask($el);
					}
				});
				
			});
		},
		close : function(params, context) {
			return this.each(function() {
				var $el = $(this);
				var opt = $el.data('lvClipMask');
				setClip($el, 0, opt.container.width(), opt.container.height(), 0);
				TweenMax.to($el, opt.animationTime, {
					clip: getClipStr($el, opt.container.height(), opt.container.width(), opt.container.height(), 0),
					ease: opt.ease,
					onComplete : function() {
						if (params !== undefined) {
							params.onComplete.call(context, params.onCompleteParams);
						}
						removeMask($el);
					}
				});
			});
		},
		destroy : function() {
			return this.each(function() {
				var $t = $(this);
				removeMask($t);
			});
		}
	};

	$.fn.lvClipMask = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if ( typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.position');
		}

	};
	$.fn.lvClipMask.defaults = {
		container : null,
		animationTime : 1,
		ease : Circ.easeInOut
	};
})(jQuery);
