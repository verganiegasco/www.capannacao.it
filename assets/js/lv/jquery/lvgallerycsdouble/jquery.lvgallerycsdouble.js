/**
 * 2012-04-14: introdotta dipendenza da LV.Size;
 *
 *
 * @version 1.1
 * Introdotta scala
 * Introddo cambio dinamico dei valori
 */

(function($) {
	
	function _posElements($el, opt) {
			$el.find(opt.itemClass+':not(.active)').each(function(i) {
				var $t = $(this);
				switch(opt.type) {
					case 'H':
						var left = (i % 2 === 0) ? -$t.width() : $t.width();
						left = left*opt.direction;
						$t.css({
							left : left
						});
						break;
					case 'V':
						var top = (i % 2 === 0) ? -$t.height() : $t.height();
						top = top*opt.direction;
						$t.css({
							top : top
						});
						break;
				}
			});
	}

	var methods = {
		init : function(opts) {
			return this.each(function() {
				var opt = $.extend({}, $.fn.lvGalleryCSDouble.defaults, opts);
				var $el = $(this);
				
				opt._count = $el.find(opt.itemClass).length;
				opt._current = 0;
				opt._prev = 0;
				$el.addClass('jqlvGalleryCSDouble');
				$($el.find(opt.itemClass).get(opt._current)).addClass('active');
				
				$(window).bind('resize.jqlvGalleryCSDouble', function() {
					_posElements($el, opt);
				});
				
				_posElements($el, opt);
				
				$el.data('jqlvGalleryCSDouble', opt);

			});

		},
		play: function() {
			return this.each(function() {
				var $self = $(this);
				var opt = $self.data('jqlvGalleryCSDouble');
				opt.intervalID = setInterval(function() {
					$self.lvGalleryCSDouble('next');
				}, opt.time*1000);
				$self.data('jqlvGalleryCSDouble', opt);
			});
		},
		next: function() {
			return this.each(function() {
				var $self = $(this);
				var opt = $self.data('jqlvGalleryCSDouble');
				opt._prev = opt._current;
				if(opt._current < opt._count-1) {
					opt._current++;
				} else {
					opt._current = 0;
				}
				
				$self.lvGalleryCSDouble('animate');
				$self.data('jqlvGalleryCSDouble', opt);
			});
		},
		animate: function() {
			return this.each(function() {
				var $self = $(this);
				var opt = $self.data('jqlvGalleryCSDouble');
				$self.find(opt.itemClass).each(function(index) {
					var $t = $(this);
					$t.removeClass('active');
				});
				$($self.find(opt.itemClass).get(opt._current)).addClass('active');
				$self.lvCSDouble({
					$active : $($self.find(opt.itemClass).get(opt._current)),
					$prev : $($self.find(opt.itemClass).get(opt._prev)),
					animationTime : opt.animationTime,
					nSlices : opt.nSlices,
					ease : opt.ease,
					type : opt.type,
					direction: opt.direction
				}).lvCSDouble('animate', {
					onComplete : function() {
						$self.lvCSDouble('destroy');
					}
				});
				$self.data('jqlvGalleryCSDouble', opt);
			});
		}
	};

	$.fn.lvGalleryCSDouble = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if ( typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.lvGalleryCSDouble');
		}

	};
	$.fn.lvGalleryCSDouble.defaults = {
		time: 3,
		cycle: true,
		itemClass: '.preview',
		animationTime : 1,
		nSlices : 2,
		ease : Circ.easeInOut,
		type : 'V',
		direction: 1
	};
})(jQuery);
