;(function($) {

	var counter = 0;

	function createVign($t, color, zIndex) {
		var line = $('<span/>');
		var opt = $t.data('lvLines');
		line.css({
			position : 'absolute',
			display : 'block',
			'background-color' : color
		});
		if(!isNaN(zIndex)) {
			line.css({
				'z-index' : zIndex
			});
		}
		line.addClass('lvLine');
		return line;
	}

	var methods = {
		init : function(opts) {

			return this.each(function() {
				var opt = $.extend({}, $.fn.lvLines.defaults, opts);
				counter+=1;
				var $t = $(this);
				if (!opt.outColor) {
					opt.outColor = $t.css('color');
				}

				opt.linesOutW = [];
				opt.linesOutH = [];
				opt.linesOverW = [];
				opt.linesOverH = [];

				
				if(opt.removeBorder) {
					$t.css({
						border: 'none'
					});
				}

				opt.id = 'lvLines-' + counter;

				var top = createVign($t, opt.outColor, opt.zIndex);
				top.css({
					width : "100%",
					height : opt.strokeWidth,
					left : "0%",
					top : 0
				});

				var right = createVign($t, opt.outColor, opt.zIndex);
				right.css({
					width : opt.strokeWidth,
					height : "100%",
					right : "0",
					top : "0%"
				});

				var bottom = createVign($t, opt.outColor, opt.zIndex);
				bottom.css({
					width : "100%",
					height : opt.strokeWidth,
					right : "0%",
					bottom : 0
				});

				var left = createVign($t, opt.outColor, opt.zIndex);
				left.css({
					width : opt.strokeWidth,
					height : "100%",
					left : 0,
					bottom : "0%"
				});

				$t.prepend(top);
				$t.prepend(right);
				$t.prepend(bottom);
				$t.prepend(left);

				opt.linesOutW.push(top);
				opt.linesOutH.push(right);
				opt.linesOutW.push(bottom);
				opt.linesOutH.push(left);

				var top2 = createVign($t, opt.overColor, opt.zIndex);
				top2.css({
					width : "0%",
					height : opt.strokeWidth,
					right : "0%",
					top : 0
				});

				var right2 = createVign($t, opt.overColor, opt.zIndex);
				right2.css({
					width : opt.strokeWidth,
					height : "0%",
					right : "0",
					bottom : "0%"
				});

				var bottom2 = createVign($t, opt.overColor, opt.zIndex);
				bottom2.css({
					width : "0%",
					height : opt.strokeWidth,
					left : "0%",
					bottom : 0
				});

				var left2 = createVign($t, opt.overColor, opt.zIndex);
				left2.css({
					width : opt.strokeWidth,
					height : "0%",
					left : 0,
					top : "0%"
				});

				$t.prepend(top2);
				$t.prepend(right2);
				$t.prepend(bottom2);
				$t.prepend(left2);

				opt.linesOverW.push(top2);
				opt.linesOverH.push(right2);
				opt.linesOverW.push(bottom2);
				opt.linesOverH.push(left2);

				if (opt.auto) {
					$t.lvLines('initListeners');
				}

				opt.anim = [];
				$t.data('lvLines', opt);
				$t.lvLines(opt.startStatus);

			});
		},
		out : function(params) {
			return this.each(function() {
				var $t = $(this);
				var opt = $t.data('lvLines');
				if (opt.anim) {
					$.each(opt.anim, function(index) {
						this.kill();
					});
				}
				var t1 = TweenMax.to(opt.linesOutW, opt.animationTime, {
					width : "100%",
					delay : opt.delay,
					ease : opt.ease
				});
				var t2 = TweenMax.to(opt.linesOutH, opt.animationTime, {
					height : "100%",
					delay : opt.delay,
					ease : opt.ease,
					onComplete : function() {
						if (params !== undefined) {
							params.onComplete.call(params.context, params.onCompleteParams);
						}
					}
				});

				var t3 = TweenMax.to(opt.linesOverW, opt.animationTime, {
					width : "0%",
					ease : opt.ease
				});
				var t4 = TweenMax.to(opt.linesOverH, opt.animationTime, {
					height : "0%",
					ease : opt.ease
				});
				opt.anim = [t1, t2, t3, t4];
				$t.data('lvLines', opt);
			});
		},
		over : function(params) {
			return this.each(function() {
				var $t = $(this);
				var opt = $t.data('lvLines');
				if (opt.anim) {
					$.each(opt.anim, function(index) {
						this.kill();
					});
				}
				var t1 = TweenMax.to(opt.linesOutW, opt.animationTime, {
					width : "0%",
					ease : opt.ease
				});
				var t2 = TweenMax.to(opt.linesOutH, opt.animationTime, {
					height : "0%",
					ease : opt.ease
				});

				var t3 = TweenMax.to(opt.linesOverW, opt.animationTime, {
					width : "100%",
					delay : opt.delay,
					ease : opt.ease
				});
				var t4 = TweenMax.to(opt.linesOverH, opt.animationTime - 0.1, {
					height : "100%",
					delay : opt.delay,
					ease : opt.ease,
					onComplete : function() {
						if (params !== undefined) {
							params.onComplete.call(params.context, params.onCompleteParams);
						}
					}
				});
				opt.anim = [t1, t2, t3, t4];
				$t.data('lvLines', opt);

			});
		},
		destroy : function() {
		},
		initListeners : function() {
			return this.each(function() {
				var $t = $(this);
				var opt = $t.data('lvLines');
				$t.hover(function() {
					if (!$t.hasClass('active')) {
						$t.lvLines('over');
					}
				}, function() {
					if (!$t.hasClass('active')) {
						$t.lvLines('out');
					}
				});
			});
		}
	};

	$.fn.lvLines = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if ( typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.position');
		}

	};
	$.fn.lvLines.defaults = {
		outColor : null,
		overColor : "#000000",
		animationTime : 0.5,
		selectedClass : 'active',
		auto : true,
		strokeWidth : 1,
		delay : 0.2,
		ease : Sine.easeInOut,
		auto : true,
		startStatus : 'out',
		removeBorder: true,
		zIndex: NaN
	};
})(jQuery);
