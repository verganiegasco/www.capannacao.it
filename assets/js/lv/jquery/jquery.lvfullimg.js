(function($) {
	
	var _alias = $.fn.lvFullImg;
	
	var methods = {
		init : function(opts) {
			
			var options = $.extend({}, $.fn.lvFullImg.defaults, opts);
			
			return this.each(function() {
				$(this).data('options', options);
				$(this).data('isLoaded', false);
				if(options.autoload==true) {
					$(this).lvFullImg('load');
				}
			});

		},
		destroy :function() {
		},
		resize:function() {
			return this.each(function() {
				var options = $(this).data('options');
				var $img = $(this);
				if($(this).data('isLoaded')) {
					resizeImg(options);
				}
				
				function resizeImg(options) {
					
					var w = options.container.width();
					var h = options.container.height();
					try {
						var sizeObj = new LV.Size($img.data('srcWidth'), $img.data('srcHeight'));
						var size = sizeObj.fit(options.fitType, w*options.resizePercX, h*options.resizePercY);
					} catch(e) {
						alert(e);
					}
					var x = 0;
					var y = 0;
					var position = new LV.Position();
					var point = position.getPosition(options.alignment, size.width, size.height, options.container.width(), options.container.height());
					$img.css({
						width: size.width,
						height: size.height,
						left: point.x,
						top: point.y
					});
					
				}
			});
		},
		load: function() {
			return this.each(function() {
				if(!$(this).data('isLoaded')) {
					var options = $(this).data('options');
					var animation = options.animation;
					var $window = $(window);
					var $img = $(this);
					var $source = $img.attr('data-source');
					$img.css({
						opacity: 0
					});
					$img.attr('src', $source);
					$img.load(function(){
						if(animation!=null) {
							$img.css(animation.startCss).delay(animation.delay).animate(animation.endCss, animation.time);
						}
						$img.css({
							'z-index': 1000,
							position: 'absolute',
							top: '0px',
							left: '0px',
							overflow: 'hidden'
						});
						$img.data('srcWidth', $img.width());
						$img.data('srcHeight', $img.height());
						if(options.autoresize) {
							$window.bind('resize.vegFullImg',function(e){
								$img.lvFullImg('resize');
							});
						}
						$img.data('isLoaded', true);
						$img.lvFullImg('resize');
						options.handleLoaded();
					});
				}
								
			});
		}
	};


	$.fn.lvFullImg = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(
					arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.position');
		}

	};
	$.fn.lvFullImg.defaults = {
			container: null,
			handleReposition : function(nx, ny) {
				return {
					x : nx,
					y : ny
				};
			},
			handleLoaded: function() {

			},
			autoresize: true,
			alignment: 'CC',
			autoload: true,
			fitType: 'fitOut',
			resizePerc: 1.0,
			resizePercX: 1.0,
			resizePercY: 1.0,
			animation: {
				delay: 0,
				time: 500,
				startCss: {
					opacity: 0
				},
				endCss: {
					opacity: 1
				}
			}
	};
})(jQuery);