(function($) {

	var methods = {
		init : function(opts) {

			var opt = $.extend({}, $.fn.lvBtnArrow.defaults, opts);

			return this.each(function() {
				var $t = $(this);
				/*if (!opt.container) {
				 opt.container = $t;
				 }*/
				if (!opt.containerHeight) {
					opt.containerHeight = $t.height();
				}
				if (!opt.containerWidth) {
					opt.containerWidth = $t.width();
				}
				var $container = $('<div class="lvBtnArrowCont"></div>');
				var $b1 = $('<div class="b1 brace"></div>');
				var $b2 = $('<div class="b2 brace"></div>');

				$container.css({
					'position' : 'absolute',
					'height' : opt.containerHeight,
					'width' : opt.containerWidth,
					'overflow' : 'hidden',
					top : 0,
					left : 0
				});

				$b1.css({
					'background' : opt.outColor,
					'position' : 'absolute',
					'transform' : 'rotate(' + opt.deg + 'deg)'
				});
				$b2.css({
					'background' : opt.outColor,
					'position' : 'absolute',
					'transform' : 'rotate(-' + opt.deg + 'deg)'
				});

				var left = opt.containerWidth * .5;
				var top = opt.containerHeight * .5;

				if (opt.type == 'DOWN' || opt.type == 'UP') {
					$b1.css({
						'height' : opt.braceHeight,
						'width' : opt.braceWidth
					});
					$b2.css({
						'height' : opt.braceHeight,
						'width' : opt.braceWidth
					});

				}
				if (opt.type == 'RIGHT' || opt.type == 'LEFT' || opt.type == "X") {
					$b1.css({
						'height' : opt.braceWidth,
						'width' : opt.braceHeight
					});
					$b2.css({
						'height' : opt.braceWidth,
						'width' : opt.braceHeight
					});

				}

				if (opt.type == 'DOWN') {
					$b1.css({
						'bottom' : 0,
						'left' : left,
						'transform-origin' : 'center bottom 0'
					});
					$b2.css({
						'bottom' : 0,
						'left' : left,
						'transform-origin' : 'center bottom 0'
					});
				}

				if (opt.type == 'UP') {
					$b1.css({
						'top' : 0,
						'left' : left,
						'transform-origin' : 'center top 0'
					});
					$b2.css({
						'top' : 0,
						'left' : left,
						'transform-origin' : 'center top 0'
					});
				}

				if (opt.type == 'RIGHT') {
					var cat = opt.braceHeight * Math.cos(opt.deg * Math.PI / 180);
					var x = (opt.containerWidth - cat) * .5;
					$b1.css({
						'right' : x,
						'top' : top,
						'transform-origin' : 'right center 0'
					});
					$b2.css({
						'right' : x,
						'top' : top,
						'transform-origin' : 'right center 0'
					});
				}
				if (opt.type == 'LEFT') {
					var cat = opt.braceHeight * Math.cos(opt.deg * Math.PI / 180);
					var x = (opt.containerWidth - cat) * .5;
					$b1.css({
						'left' : x,
						'top' : top,
						'transform-origin' : 'left center 0'
					});
					$b2.css({
						'left' : x,
						'top' : top,
						'transform-origin' : 'left center 0'
					});
				}
				if (opt.type == 'X') {
					$b1.css({
						'left' : (opt.containerWidth - $b1.width()) * .5,
						'top' : (opt.containerHeight - $b1.height()) * .5,
						'transform-origin' : 'center center 0'
					});
					$b2.css({
						'left' : (opt.containerWidth - $b1.width()) * .5,
						'top' : (opt.containerHeight - $b1.height()) * .5,
						'transform-origin' : 'center center 0'
					});
				}

				$container.append($b1);
				$container.append($b2);
				$t.html($container);
				/*
				 $t.width(opt.containerWidth);
				 $t.height(opt.containerHeight);
				 */
				$t.hover(function() {
					if (!$t.hasClass('active')) {
						$t.lvBtnArrow('over');
					}
				}, function() {
					if (!$t.hasClass('active')) {
						$t.lvBtnArrow('out');
					}
				});
				opt.$b1 = $b1;
				opt.$b2 = $b2;
				opt.$cont = $container;
				$t.data('lvBtnArrow', opt);
			});
		},
		over : function(params) {
			return this.each(function() {
				var $t = $(this);
				var opt = $t.data('lvBtnArrow');
				if (opt.animationType === 'rotation') {
					TweenMax.to(opt.$cont, opt.animationTime, {
						rotation : 90,
						ease : opt.ease,
						onComplete : function() {
							if (params !== undefined) {
								params.onComplete.call(context, params.onCompleteParams);
							}
						}
					});
				} else {
					TweenMax.to(opt.$b1, opt.animationTime, {
						rotation : -opt.deg,
						ease : opt.ease,
						onComplete : function() {
							if (params !== undefined) {
								params.onComplete.call(context, params.onCompleteParams);
							}
						}
					});
					TweenMax.to(opt.$b2, opt.animationTime, {
						rotation : opt.deg,
						ease : opt.ease,
						onComplete : function() {
						}
					});

				}
				TweenMax.to($t.find('.brace'), opt.animationTime, {
					css : {
						'backgroundColor' : opt.overColor
					},
					ease : opt.ease
				});
			});
		},
		out : function(params) {
			return this.each(function() {
				var $t = $(this);
				var opt = $t.data('lvBtnArrow');
				if (opt.animationType == 'rotation') {
					TweenMax.to(opt.$cont, opt.animationTime, {
						rotation : 0,
						ease : opt.ease,
						onComplete : function() {
							if (params !== undefined) {
								params.onComplete.call(context, params.onCompleteParams);
							}
						}
					});
				} else {
					TweenMax.to(opt.$b1, opt.animationTime, {
						rotation : opt.deg,
						ease : opt.ease,
						onComplete : function() {
							if (params !== undefined) {
								params.onComplete.call(context, params.onCompleteParams);
							}
						}
					});
					TweenMax.to(opt.$b2, opt.animationTime, {
						rotation : -opt.deg,
						ease : opt.ease,
						onComplete : function() {
						}
					});
				}
				TweenMax.to($t.find('.brace'), opt.animationTime, {
					css : {
						'backgroundColor' : opt.outColor
					},
					ease : opt.ease
				});

			});
		},
		destroy : function() {
			return this.each(function() {
				var $t = $(this);
				var opt = $t.data('lvBtnArrow');
				var $c = opt.clone;
				removeMask($t);
				removeMask($c);
				$c.remove();
			});
		}
	};

	$.fn.lvBtnArrow = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if ( typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.position');
		}

	};
	$.fn.lvBtnArrow.defaults = {
		braceWidth : 1,
		braceHeight : 10,
		deg : 45,
		container : null,
		animationTime : .3,
		animationType : 'default',
		overColor : '#E03957',
		outColor : '#000000',
		ease : Linear.easeNone,
		containerWidth : null,
		containerHeight : null,
		type : 'X'
	};
})(jQuery);
