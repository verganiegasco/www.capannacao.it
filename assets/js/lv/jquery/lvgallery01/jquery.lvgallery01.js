/**
 * 2012-04-14: introdotta dipendenza da LV.Size;
 *
 *
 * @version 1.1
 * Introdotta scala
 * Introddo cambio dinamico dei valori
 */

(function($) {
	
	function _posElements($el, opt) {

	}

	var methods = {
		init : function(opts) {
			return this.each(function() {
				var opt = $.extend({}, $.fn.lvGallery01.defaults, opts);
				var $el = $(this);
				
				opt._count = $el.find(opt.itemClass).length;
				opt._current = 0;
				opt._prev = 0;
				$el.addClass('jqlvGallery01');
				$($el.find(opt.itemClass)).addClass('non-active');
				$($el.find(opt.itemClass).get(opt._current)).removeClass('non-active');
				$($el.find(opt.itemClass).get(opt._current)).addClass('active');
				$el.find(opt.itemClass).each(function(index) {
					var $t = $(this);
					$t.append('<div class="overlay"></div>');
				});
				$el.find('.overlay').css({
					opacity: 0
				});
				
				$(window).bind('resize.jqlvGallery01', function() {
					_posElements($el, opt);
				});
				
				_posElements($el, opt);
				
				$el.data('jqlvGallery01', opt);

			});

		},
		play: function() {
			return this.each(function() {
				var $self = $(this);
				var opt = $self.data('jqlvGallery01');
				opt.intervalID = setTimeout(function() {
					$self.lvGallery01('next');
				}, opt.time*1000);
				opt.onTimerStart(opt.time);
				$self.data('jqlvGallery01', opt);
			});
		},
		next: function() {
			return this.each(function() {
				var $self = $(this);
				var opt = $self.data('jqlvGallery01');
				opt._prev = opt._current;
				if(opt._current < opt._count-1) {
					opt._current++;
				} else {
					opt._current = 0;
				}
				
				$self.lvGallery01('animate');
				$self.data('jqlvGallery01', opt);
			});
		},
		animate: function() {
			return this.each(function() {
				var $self = $(this);
				var opt = $self.data('jqlvGallery01');
				opt.onTimerComplete();
				$self.find(opt.itemClass).each(function(index) {
					var $t = $(this);
					$t.addClass('non-active');
					$t.removeClass('active');
				});

				$active = $($self.find(opt.itemClass).get(opt._current));
				$active.removeClass('non-active');
				$active.addClass('active');
				$prev = $($self.find(opt.itemClass).get(opt._prev)); 
				
				TweenMax.to($prev.find('.overlay'), .3, {
					opacity: .5
				});			
				
				TweenMax.to($prev, .3, {
					css: {
						z: -500
					},
					onComplete: function() {

					},
					ease: Sine.easeInOut
				});

				TweenMax.to($prev, .3, {
					css : {
						x : -$prev.width() * .5,
						rotationY : 45
					},
					ease : Sine.easeInOut,
					delay: .25
				}); 

				TweenMax.to($active, 0, {
					css: {
						x: $self.width()*1.2,
						z: -500,
						rotationY: -45
					},
					immediateRender: true
				});
				TweenMax.to($active.find('.overlay'), 0, {
					opacity: .5,
					immediateRender: true
				}); 

				TweenMax.to($active, .7, {
					css: {
						x : 0,
						rotationY : 0,
						rotationX : 0
					},
					delay : .2,
					ease : Circ.easeInOut
				});
				TweenMax.to($active, .7, {
					css: {
						z : 0
					},
					delay : .7,
					ease : Circ.easeInOut,
					onComplete: function() {
						$self.lvGallery01('play');
					}
				});
				TweenMax.to($active.find('.overlay'), .3, {
					opacity: 0,
					delay: .6
				});  

				$self.data('jqlvGallery01', opt);
			});
		},
		destroy: function() {
			return this.each(function() {
				var $self = $(this);
				var opt = $self.data('jqlvGallery01');
				clearInterval(opt.intervalID);
			});
		}
	};

	$.fn.lvGallery01 = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if ( typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.lvGallery01');
		}

	};
	$.fn.lvGallery01.defaults = {
		time: 3,
		cycle: true,
		itemClass: '.preview',
		animationTime : 1,
		nSlices : 2,
		ease : Circ.easeInOut,
		type : 'V',
		direction: 1,
		intervalID: null,
		onTimerStart: function(time) {
			
		},
		onTimerComplete: function() {
			
		}
	};
})(jQuery);
