(function($) {
	
	var counter = 0;
	
	function createSlice($t, type) {
		var slice = $('<div/>');
		var bg = $('<div class="bg"/>');
		var opt = $t.data('lvImgSlices');
		
		if(type=='H') {
			slice.css({
				position: 'absolute',
				display: 'block',
				height: '50%',
				width: '100%',
				overflow: 'hidden'
			});
			
			bg.css({
				'background': "url('"+$t.attr('src')+"') center center no-repeat",
				'background-size': "cover",
				height: '200%',
				width: '100%',
				position: 'absolute'
			});
		}
		
		if(type=='V') {
			slice.css({
				position: 'absolute',
				display: 'block',
				height: '100%',
				width: '50%',
				overflow: 'hidden'
			});
			
			bg.css({
				'background': "url('"+$t.attr('src')+"') center center no-repeat",
				'background-size': "cover",
				height: '100%',
				width: '200%',
				position: 'absolute'
			});
		}
		
		
		bg.addClass('bg');
		slice.append(bg);
		
		slice.addClass('lvImgSlice');
		return slice;
	}

	var methods = {
		init : function(opts) {


			return this.each(function() {
				var opt = $.extend({}, $.fn.lvImgSlices.defaults, opts);
				var $t = $(this);
				var $over = $($t.find(opt.overClass));
				var $out = $($t.find(opt.outClass));
				var $slices = $('<div class="lvImgSlices"></div>');
				var $container = $t;
				$slices.css({
					display: 'block',
					position: 'absolute',
					height: "100%",
					width: "100%",
					overflow: 'hidden'
				});
				
				
				var $slice1 = createSlice($out, opt.type);
				var $slice2 = createSlice($out, opt.type);
				var $slice3 = createSlice($over, opt.type);
				var $slice4 = createSlice($over, opt.type);
				
				
				if(opt.type=='H') {
					$slice1.css({
						top: 0
					});
					$slice2.css({
						bottom: 0
					});
					$slice2.find('.bg').css({
						top: '-100%'
					});
					$slice3.css({
						top: 0,
						left: "100%"
					});
					$slice4.css({
						bottom: 0,
						left: '-100%'
					});
					$slice4.find('.bg').css({
						top: '-100%'
					});
				}
				
				if(opt.type=='V') {
					$slice1.css({
						left: 0
					});
					$slice2.css({
						right: 0
					});
					$slice2.find('.bg').css({
						left: '-100%'
					});
					$slice3.css({
						left: 0,
						top: "100%"
					});
					$slice4.css({
						right: 0,
						top: '-100%'
					});
					$slice4.find('.bg').css({
						left: '-100%'
					});
				}
				
				
				$slices.append($slice1);
				$slices.append($slice2);
				$slices.append($slice3);
				$slices.append($slice4);
				
				$t.prepend($slices);
				
				$over.css({
					display: 'none'
				});
				$out.css({
					display: 'none'
				});
				
				opt.slicesCont = $slices;
				
				opt.slices = [];
				opt.slices.push($slice1);
				opt.slices.push($slice2);
				opt.slices.push($slice3);
				opt.slices.push($slice4);
				
				
				$t.data('lvImgSlices', opt);
				
				$container.hover(function() {
					if(!$t.hasClass('active')) {
						$t.lvImgSlices('over');
					}
				}, function() {
					if(!$t.hasClass('active')) {
						$t.lvImgSlices('out');
					}
				});
				
			});
		},
		out: function() {
			return this.each(function() {
				var $t = $(this);
				var opt = $t.data('lvImgSlices');
				var slices = opt.slices;	
				if(opt.type=='H') {			
					TweenMax.to(slices[0], opt.animationTime, {left: "0%", ease:opt.ease});
					TweenMax.to(slices[1], opt.animationTime, {left: "0%", ease:opt.ease});
					TweenMax.to(slices[2], opt.animationTime, {left: "100%", ease:opt.ease});
					TweenMax.to(slices[3], opt.animationTime, {left: "-100%", ease:opt.ease});
				}
				if(opt.type=='V') {			
					TweenMax.to(slices[0], opt.animationTime, {top: "0%", ease:opt.ease});
					TweenMax.to(slices[1], opt.animationTime, {top: "0%", ease:opt.ease});
					TweenMax.to(slices[2], opt.animationTime, {top: "100%", ease:opt.ease});
					TweenMax.to(slices[3], opt.animationTime, {top: "-100%", ease:opt.ease});
				}
				
				$t.data('lvImgSlices', opt);
			});
		},
		over: function() {
			return this.each(function() {
				var $t = $(this);
				var opt = $t.data('lvImgSlices');
				$t.data('lvImgSlices', opt);
				var slices = opt.slices;
				
				if(opt.type=='H') {
					TweenMax.to(slices[0], opt.animationTime, {left: "-100%", ease:opt.ease});
					TweenMax.to(slices[1], opt.animationTime, {left: "100%", ease:opt.ease});
					TweenMax.to(slices[2], opt.animationTime, {left: "0%", ease:opt.ease});
					TweenMax.to(slices[3], opt.animationTime, {left: "0%", ease:opt.ease});
				}
				if(opt.type=='V') {
					TweenMax.to(slices[0], opt.animationTime, {top: "-100%", ease:opt.ease});
					TweenMax.to(slices[1], opt.animationTime, {top: "100%", ease:opt.ease});
					TweenMax.to(slices[2], opt.animationTime, {top: "0%", ease:opt.ease});
					TweenMax.to(slices[3], opt.animationTime, {top: "0%", ease:opt.ease});
				}
				
			});
		},
		destroy : function() {
		}
	};

	$.fn.lvImgSlices = function(method) {
		if(methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if( typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.position');
		}

	};
	$.fn.lvImgSlices.defaults = {
		overClass: null,
		outClass: null,
		animationTime: .5,
		selectedClass: 'active',
		auto: true,
		delay: .2,
		ease: Circ.easeInOut,
		type: 'V'
	};
})(jQuery);
