/**
 * 2012-04-14: introdotta dipendenza da LV.Size;
 *
 *
 * @version 1.1
 * Introdotta scala
 * Introddo cambio dinamico dei valori
 */

(function($) {

	function calc($el, opt, w, h, fitType) {
		var size = new LV.Size(opt.originalWidth, opt.originalHeight);
		var container = $(window);
		if (opt.container != null) {
			container = opt.container;
		}
		if (opt.useParent) {
			container = $el.parent();
		}
		var containerWidth = container.width() + opt.offsetX;
		var containerHeight = container.height() + opt.offsetY;
		
		if (w) {
			containerWidth = w;
		}
		if (h) {
			containerHeight = h;
		}
		containerWidth *= opt.scale;
		containerHeight *= opt.scale;
		var rect = {};
		if (fitType) {
			opt.fitType = fitType;
		}
		if (opt.fitType == 'fitIn') {
			rect = size.fitIn(containerWidth, containerHeight);
		}
		if (opt.fitType == 'fitOut') {
			rect = size.fitOut(containerWidth, containerHeight);
		}
		if (opt.fitType == 'fitWidth') {
			rect = size.fitWidth(containerWidth, containerHeight);
		}
		return rect;
	}

	var methods = {
		init : function(opts) {
			return this.each(function() {
				var options = $.extend({}, $.fn.lvResize.defaults, opts);
				var $el = $(this);
				$el.addClass('jqLvResize');
				options.originalWidth = $el.attr('width') ? $el.attr('width') : parseInt($(this).css('width'));
				options.originalHeight = $el.attr('height') ? $el.attr('height') : parseInt($(this).css('height'));
				
				if ($(this).attr('data-fittype') && $(this).attr('data-fittype') != "") {
					options.fitType = $(this).attr('data-fittype');
				}
				$el.data('optionsLvResize', options);

				$el.lvResize('resize');
				if (options.auto) {
					$(window).bind("resize.lvResize", function() {
						$el.lvResize('resize');
					});
				}
			});

		},
		resize : function(w, h, fitType) {
			return this.each(function() {
				var opt = $(this).data('optionsLvResize');
				if (!opt)
					return false;
				var $el = $(this);
				var rect = calc($el, opt, w, h, fitType);
				$el.css({
					width : rect.width,
					height : rect.height
				});
				if ($el.attr("data-position") || opt.align) {
					var pos = opt.align || $el.attr("data-position");
					var position = new LV.Position();
					var point = position.getPosition(pos, $el.width(), $el.height(), containerWidth, containerHeight);
					var x = !isNaN(point.x) ? point.x : parseInt($el.css('left'));
					var y = !isNaN(point.y) ? point.y : parseInt($el.css('top'));

					$el.css({
						top : y,
						left : x
					});
				}
			});
		},
		calculate: function(w, h, fitType) {
			var rect;
			this.each(function() {
				var opt = $(this).data('optionsLvResize');
				if (!opt)
					return false;
				var $el = $(this);
				rect = calc($el, opt,w, h, fitType);
			});
			return rect;
		},
		getFitType : function() {
			var opt = $(this).data('optionsLvResize');
			if (!opt)
				return false;
			return opt.fitType;
		},
		updateOption : function(key, value) {
			return this.each(function() {
				if (value) {
					var opt = $(this).data('optionsLvResize');
					opt[key] = value;
					$(this).data('optionsLvResize', opt);
				} else {

				}
			});
		},
		reset : function(w, h, fitType) {
			return this.each(function() {
				var opt = $(this).data('optionsLvResize');
				if (!opt)
					return false;
				var $el = $(this);
				$el.css({
					width : opt.originalWidth,
					height : opt.originalHeight
				});
			});
		},
		destroy : function() {
			return this.each(function() {
				var $el = $(this);
				var opt = $el.data('optionsLvResize');
				if (!opt)
					return false;
				$(window).unbind('resize.lvResize');
				$(this).data('optionsLvResize', null);
			});
		}
	};

	$.fn.lvResize = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if ( typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.lvResize');
		}

	};
	$.fn.lvResize.defaults = {
		fitType : 'fitOut',
		container : null,
		handleResized : function(nx, ny) {
			return {
				x : nx,
				y : ny
			};
		},
		handleResize : function() {

		},
		align : null,
		offsetX : 0,
		offsetY : 0,
		useParent : false,
		auto : true,
		scale : 1
	};
})(jQuery); 