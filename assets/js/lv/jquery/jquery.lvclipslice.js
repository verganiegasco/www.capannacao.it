(function($) {

	var counter = 0;

	function getClipStr(T, R, B, L) {
		var rect = 'rect(' + T + 'px ' + R + 'px ' + B + 'px ' + L + 'px)';
		return rect;
	}

	function setClip($el, T, R, B, L) {
		$el.css({
			clip : getClipStr(T, R, B, L)
		});
	}

	function removeMask($el) {
		$el.css({
			clip : 'auto'
		});
	}

	var methods = {
		init : function(opts) {

			var opt = $.extend({}, $.fn.lvClipSlice.defaults, opts);

			return this.each(function() {
				counter++;
				var $el = $(this);
				if (!opt.container) {
					opt.container = $el;
				}
				opt.slices = [];
				opt.slices.push($el);
				for (var i = 0; i < opt.nSlices - 1; i++) {
					var $c = $el.clone();
					$c.addClass('clone').appendTo($el.parent());
					$c.addClass('lvClipSlice');
					$c.addClass('lvClipSlice-' + counter);
					opt.slices.push($c);
				};

				$el.addClass('lvClipSlice');
				$el.addClass('lvClipSlice-' + counter);

				opt.id = 'lvClipSlice-' + counter;
				opt.clone = $c;
				$el.data('lvClipSlice', opt);
			});
		},
		createClosed: function() {
			return this.each(function() {
				var $el = $(this);
				var opt = $el.data('lvClipSlice');
				var w = opt.container.width() / opt.nSlices;
				for (var i = 0, j = opt.slices.length; i < j; i++) {
					var $s = opt.slices[i];
					var h = i%2==0? opt.container.height():0;
					$s.addClass('slice-'+i);
					setClip($s, h, (w * i) + w, h, w * i);
				};
			});
		},
		createOpened: function(type) {
			return this.each(function() {
				var $el = $(this);
				var opt = $el.data('lvClipSlice');
				if(type!==undefined) {
					opt.type = type;
				}
				if(opt.type == 'V') {
					var w = opt.container.width() / opt.nSlices;
					for (var i = 0, j = opt.slices.length; i < j; i++) {
						var $s = opt.slices[i];
						$s.addClass('slice-'+i);
						setClip($s, 0, (w * i) + w, opt.container.height(), w * i);
					};	
				}
				if(opt.type == 'H') {
					var w = opt.container.height() / opt.nSlices;
					for (var i = 0, j = opt.slices.length; i < j; i++) {
						var $s = opt.slices[i];
						$s.addClass('slice-'+i);
						setClip($s, w*i, opt.container.width(), (w * i) + w, 0);
					};	
				}

			});
		},
		getSlice: function(index) {
			var $el = this;
			var opt = $el.data('lvClipSlice');
			$s = opt.slices[index];
			return $s;
		},
		getSlices: function() {
			var $el = this;
			var opt = $el.data('lvClipSlice');
			return opt.slices;
		},
		open : function(params, context) {
			return this.each(function() {
				var $el = $(this);
				var opt = $el.data('lvClipSlice');
				var w = opt.container.width() / opt.nSlices;
				$el.lvClipSlice('createClosed');
				for (var i = 0, j = opt.slices.length; i < j; i++) {
					var $s = opt.slices[i];
					if(i%2==0) {
						$s.addClass('clone-even');
					} else {
						$s.addClass('clone-odd');
					}
					TweenMax.to($s, opt.animationTime, {
						clip : getClipStr(0, (w * i) + w, opt.container.height(), w * i),
						ease : opt.ease
					});
					if (i == opt.nSlices - 1) {
						TweenMax.to($s, opt.animationTime, {
							clip : getClipStr(0, (w * i) + w, opt.container.height(), w * i),
							ease : opt.ease,
							onComplete : function() {
								if (params !== undefined) {
									params.onComplete.call(context, params.onCompleteParams);
								}
								removeMask($s);
							}
						});
					}
				}
			});
		},
		close : function(params, context) {
			return this.each(function() {
				var $el = $(this);
				var opt = $el.data('lvClipSlice');
				var w = opt.container.width() / opt.nSlices;
				$el.lvClipSlice('createOpened');
				for (var i = 0, j = opt.slices.length; i < j; i++) {
					var $s = opt.slices[i];
					if(i%2==0) {
						$s.addClass('clone-even');
					} else {
						$s.addClass('clone-odd');
					}
					var h = i%2==0? opt.container.height():0;
					TweenMax.to($s, opt.animationTime, {
						clip : getClipStr(h, (w * i) + w, h, w * i),
						ease : opt.ease
					});
					if (i == opt.nSlices - 1) {
						TweenMax.to($s, opt.animationTime, {
							clip : getClipStr(h, (w * i) + w, h, w * i),
							ease : opt.ease,
							onComplete : function() {
								if (params !== undefined && params.onComplete !== undefined) {
									params.onComplete.call(context, params.onCompleteParams);
								}
								removeMask($s);
							}
						});
					}
				}
			});
		},
		destroy : function() {
			return this.each(function() {
				var $t = $(this);
				var opt = $t.data('lvClipSlice');
				$t.removeClass('lvClipSlice');
				$t.removeClass (function (index, css) {
				    return (css.match (/\clone-\S+/g) || []).join(' ');
				});
				$t.removeClass (function (index, css) {
				    return (css.match (/\lvClipSlice-\S+/g) || []).join(' ');
				});
				$t.removeClass (function (index, css) {
				    return (css.match (/\slice-\S+/g) || []).join(' ');
				});
				for (var i = 0, j = opt.slices.length; i < j; i++) {
					var $s = opt.slices[i];
					removeMask($s);
					if ($s.hasClass('clone')) {
						$s.remove();
					}
				}
			});
		}
	};

	$.fn.lvClipSlice = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if ( typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.position');
		}

	};
	$.fn.lvClipSlice.defaults = {
		container : null,
		animationTime : 1,
		nSlices : 2,
		ease : Circ.easeInOut,
		type: 'V'
	};
})(jQuery);
