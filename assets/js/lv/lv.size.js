/**
 * @author Luigi Vergani
 */

var LV = LV || {};

LV.Size = function(width, height) {
	this.rectangle = {
		x : 0,
		y : 0,
		width : 0,
		height : 0
	};
	this.ratio = width / height;
	this.srcWidth = width;
	this.srcHeight = height;
};

LV.Size.FIT_IN = "fitIn";
LV.Size.FIT_OUT = "fitOut";
LV.Size.FIT_WIDTH = "fitWidth";

LV.Size.prototype = {
	constructor : LV.Size,
	fit : function(fitType, containerWidth, containerHeight, perc) {
		switch(fitType) {
			case LV.Size.FIT_IN:
				this.fitIn(containerWidth, containerHeight, perc);
				break;
			case LV.Size.FIT_OUT:
				this.fitOut(containerWidth, containerHeight, perc);
				break;
			case LV.Size.FIT_WIDTH:
				this.fitWidth(containerWidth, containerHeight, perc);
				break;
		}
		return this.rectangle;
	},
	fitIn : function(containerWidth, containerHeight, perc) {
		perc = perc == undefined ? 1 : perc;
		var w = containerWidth * perc;
		var h = containerHeight * perc;
		if (w / this.ratio <= h) {
			this.rectangle.width = w;
			this.rectangle.height = w / this.ratio;
		} else {
			this.rectangle.height = h;
			this.rectangle.width = h * this.ratio;
		}
		return this.rectangle;
	},
	fitOut : function(containerWidth, containerHeight, perc) {
		perc = perc == undefined ? 1 : perc;
		var w = containerWidth * perc;
		var h = containerHeight * perc;
		if (w / this.ratio >= h) {
			this.rectangle.width = w;
			this.rectangle.height = w / this.ratio;
		} else {
			this.rectangle.height = h;
			this.rectangle.width = h * this.ratio;
		}
		return this.rectangle;
	},
	fitWidth : function(containerWidth, containerHeight, perc) {
		perc = perc == undefined ? 1 : perc;
		var w = containerWidth * perc;
		var h = containerHeight * perc;
		this.rectangle.width = w;
		this.rectangle.height = w / this.ratio; 
		return this.rectangle;
	},
	scaleWidth : function(containerWidth, perc) {
		perc = perc == undefined ? 1 : perc;
		var w = containerWidth * perc;
		this.rectangle.width = w;
		this.rectangle.height = w / this.ratio;
		return this.rectangle;
	}
}; 