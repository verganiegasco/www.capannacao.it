/**
 * @author Luigi Vergani
 */
define(['jquery', 
		'underscore', 
		'backbone',
		'app/copertina/views/copertina.view',
		'app/location/views/location.view',
		'app/contacts/views/contacts.view',
		'app/commons/views/fullflexslider.view'
		], function($, _, Backbone, CopertinaView, LocationView, ContactsView, FullFlexSliderView) {
	    

		var view = new CopertinaView();
		var location = new LocationView();
		var conatcts = new ContactsView();
		view.resize();
		location.resize();
		$(window).resize(function() {
			view.resize();
			location.resize();
		});
		var fullFlexSliderView = new FullFlexSliderView({
			percHeight: 1,
			animateScale: true
		});

});

