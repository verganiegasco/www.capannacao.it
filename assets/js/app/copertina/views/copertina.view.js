/**
 * @author luigi
 */
define(['jquery', 'underscore', 'backbone'], 
		function($, _, Backbone) {
	var View = Backbone.View.extend({
		el : $('#copertina'),
		initialize : function() {
			var self = this;
			self.$('.logoCentraleHome').lvPosition({
				position : 'CC',
				offsetY : 70,
				auto: false
			});
			$(window).resize(function() {
				self.resize();
			});
			self.resize();

		},
		init : function() {
			var self = this;
			self.resize();
		},
		render : function() {

			return this;
		},
		resize : function() {
			var self = this;
			var $w = $(window);
			self.$el.css({
				height : $(window).height() - 70
			});
			self.$('.logoCentraleHome').lvPosition('position');

		}
	});

	return View;
});
