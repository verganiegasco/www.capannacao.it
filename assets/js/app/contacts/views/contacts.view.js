/**
 * @author luigi
 */
define(['jquery', 
		'underscore', 
		'backbone'
		], 
		function($, _, Backbone) {
	var View = Backbone.View.extend({
		el : $('#contacts-container'),
		map : null,
		mapCanvas : null,
		initialize : function() {
			var self = this;
			self.init();
		},
		init : function() {
			var self = this;
			self.$('form').validate({
				submitHandler: function(form) {
					var dataArr = $(form).serializeArray();
				    var dataObj = _(dataArr).reduce(function(acc, field) {
				      acc[field.name] = field.value;
				      return acc;
				    }, {});
					var data = $.param(dataObj);
					var FID = dataObj.form_id.replace('webform_client_form_', '');
					$.ajax({
						data: $(form).serialize(),
						url: App.BASE_URL+'/webform_ajax/'+FID,
						type: 'POST',
						success: function(result) {
							/*var popup = new PopupView({
								classes: 'popup-thanks',
								content: $('<div class="content thanks-message font-light">'+result.message+'</div>'),
								useScroll: false
							});
							popup.init();
							$('body').append(popup.render().$el);
							popup.centerWrapper();
							$('.popup-thanks').lvClipMask();
							$('.popup-thanks').lvClipMask('open');
							popup.resize();
							popup.on('close', function() {
								$('.popup-thanks').lvClipMask('close', {
									onComplete: function() {
										popup.destroy();
										self.$('form')[0].reset();
									}
								});
							});
							*/
							$.fancybox({
								content: 'Thanks for writing us'
							});
						}
					});
					return false;
				}
			});

			
			self.resize();
		},
		render : function() {

			return this;
		},
		resize : function() {
			var self = this;
			var $w = $(window);
		}
	});

	return View;
});
