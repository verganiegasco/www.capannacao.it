/**
 * @author Luigi Vergani
 */
define(['jquery', 
		'underscore', 
		'backbone',
		'app/collection/routers/router'
		], function($, _, Backbone, Router) {
	
		var router = new Router;
		Backbone.history.start();	
	}
);
