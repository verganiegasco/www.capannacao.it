/**
 * @author luigi
 */
define(['jquery',
		'underscore',
		'backbone'
		], function($, _, Backbone) {
	var View = Backbone.View.extend({
		el : $('.banner-header'),
		percHeight: .8,
		animateScale: false,
		isSlider: false,
		isInitialized: false,
		initialize : function(opt) {
			var self = this;
			if(typeof opt !== "undefined" && typeof opt.percHeight !== "undefined") {
				self.percHeight = opt.percHeight;
			}
			if(typeof opt !== "undefined" && typeof opt.animateScale !== "undefined") {
				self.animateScale = opt.animateScale;
			}
			self.isSlider = (self.$('li:not(.clone)').length > 1) ? true : false;

			$(window).resize(function() {
				self.resize();
			});
			if(!self.$el.hasClass('js-preload')) {
				self.init();
			} else {
				self.preloadAssets();
			}
      console.log('init');
		},
		init : function() {
			var self = this;
			self.$('.full-flexslider').flexslider({
				animation : 'slide',
				directionNav : false,
				controlNav : false,
				pauseOnHover : false,
				animationSpeed: 300,
				slideshowSpeed : 800,
				start: function(){
					self.resize();
					if(self.isSlider) {
						TweenMax.to(self.$('.full-flexslider-timer'), 7, {
							width: '100%'
						});
						if(self.animateScale) {
							TweenMax.to(self.$('li.full-flexslider-element img'), 40, {
								scale: 1.2
							});
						}
					}
				},
				before: function(){
					if(self.isSlider) {
						TweenMax.to(self.$('.full-flexslider-timer'), .8, {
							width: '0%'
						});
					}
				},
				after: function(){
					if(self.isSlider) {
						if(self.animateScale) {
							TweenMax.to(self.$('li:not(.full-flexslider-element)').find('img'), .1, {
								scale: 1
							});
							TweenMax.to(self.$('li.full-flexslider-element img'), 40, {
								scale: 1.2
							});
						}
						TweenMax.to(self.$('.full-flexslider-timer'), 7, {
							width: '100%'
						});
					}
				}
			});
			$('.full-flexslider img').lvResize({
				fitType: 'fitOut',
				auto: false,
				useParent: true
			});
			$('.full-flexslider img').lvPosition({
				position: 'CC',
				auto: false,
				useParent: true
			});
			self.isInitialized = true;
			self.resize();
		},
		preloadAssets: function() {
			var self = this;
			var data = [];
			self.$('li img[data-src]').each(function(index) {
				var $t = $(this);
				data.push({
					src : $t.attr('data-src')
				});
			});
			var preloader = new LV.PxLoaderAdapter(data);
			preloader.addEventListener('progress', function(e) {
				//$('*[data-src="'+e.item.src+'"]').attr('width', e.item.width);
				//$('*[data-src="'+e.item.src+'"]').attr('height', e.item.height);
				$('*[data-src="'+e.item.src+'"]').attr('src', e.item.src);
			});
			preloader.addEventListener('complete', function(e) {
				self.init();
			});
			preloader.load();
		},
		render : function() {

			return this;
		},
		resize: function() {
			var self = this;
			var $w = $(window);
			self.$el.css({
				height: ($w.height()) * self.percHeight
			});
			if(self.isInitialized) {
				$('.full-flexslider img').lvResize('resize');
				$('.full-flexslider img').lvPosition('position');
			}
		}
	});

	return View;
});
