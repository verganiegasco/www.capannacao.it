/**
 * @author luigi
 */
define(['jquery', 
		'underscore', 
		'backbone'
		], function($, _, Backbone) {
	var View = Backbone.View.extend({
		el : null,
		initialize : function() {
			var self = this;

		},
		init : function() {
			var self = this;
			self.resize();
		},
		render : function() {

			return this;
		},
		resize: function() {
			var self = this;

		}
	});

	return View;
});
