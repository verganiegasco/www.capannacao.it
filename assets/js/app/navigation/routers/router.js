/**
 * @author luigi
 */
// Filename: router.js
define([
  	'jquery',
  	'underscore',
  	'backbone'
], function($, _, Backbone
			){
    var Router = Backbone.Router.extend({
        initialize: function() {
        },
        routes: {
            '': 'main',
            'home': 'main',
            'prodotto/:id': 'getProdotto'
        },
        main: function () {
        	var self = this;
        },
        getProdotto: function (id, nome) {
        	var self = this;
        	var page = new ProdottoView();
        	App.CURRENT_NID = id;
        	HeaderView.setSelected('collections');
        }
    });

    return Router;
});
