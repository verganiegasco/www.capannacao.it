/**
 * @author luigi
 */
define(['jquery', 
		'underscore', 
		'backbone'
		], function($, _, Backbone) {
	var View = Backbone.View.extend({
		el : null,
		initialize : function() {
			var self = this;
			var $nav = $('<nav class="navxs"></nav>');
			$nav.append($('<div class="bg"></div>'));
			$nav.append($('<div class="content nav-content"></div>'));
			$nav.find('.content').append($('#block-system-main-menu>.content>.menu').clone());
			self.setElement($nav);
			$('body').append(self.$el);
			$('header .open-menu').click(function() {
				if(!self.$el.hasClass('open')) {
					self.openMenu();
				} else {
					self.closeMenu();
				}
			});
			self.$('.bg').click(function() {
				self.closeMenu();
			});
		},
		init : function() {
			var self = this;
			self.resize();
		},
		render : function() {

			return this;
		},
		openMenu: function() {
			var self = this;
			self.$el.addClass('open');
			self.$el.css({
				display: 'block'
			});
			TweenMax.to($('#main-content-wrapper'), .5, {
				marginLeft: 200,
				ease: Circ.easeInOut
			});
			TweenMax.to($('#main-footer-wrapper'), .5, {
				marginLeft: 200,
				ease: Circ.easeInOut
			});
		},
		closeMenu: function() {
			var self = this;
			self.$el.removeClass('open');

			TweenMax.to($('#main-content-wrapper'), .5, {
				marginLeft: 0,
				ease: Circ.easeInOut
			});
			TweenMax.to($('#main-footer-wrapper'), .5, {
				marginLeft: 0,
				ease: Circ.easeInOut,
				onComplete: function() {
					self.$el.css({
						display: 'none'
					});
				}
			});
		},
		resize: function() {
			var self = this;

		}
	});

	return View;
});
