/**
 * @author luigi
 */
define(['jquery', 'underscore', 'backbone'], function($, _, Backbone) {
	var View = Backbone.View.extend({
		el : $('#main-header-wrapper'),
		initialize : function() {
			var self = this;
			$(window).scroll(function() {
				var $t = $(this);
				self.posNav();
			});
			$(window).resize(function() {
				self.resize();
			});
			self.resize();
		},
		init : function() {
			var self = this;
			self.resize();
		},
		posNav: function() {
			var self = this;
			var $w = $(window);
			if($w.scrollTop()> $w.height()-self.$el.height()) {
				self.$el.css({
					position: 'fixed',
					top: 0
				});
			} else {
				self.$el.css({
					position: 'absolute',
					top: $w.height()- self.$el.height()
				});
			}
		},
		render : function() {

			return this;
		},
		resize : function() {
			var self = this;
			self.posNav();
		}
	});

	return View;
});
