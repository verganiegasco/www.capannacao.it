/**
 * @author luigi
 */
define(['jquery', 
		'underscore', 
		'backbone', 
		'async!http://maps.google.com/maps/api/js?sensor=false'
		], 
		function($, _, Backbone) {
	var View = Backbone.View.extend({
		el : $('#location-container'),
		map : null,
		mapCanvas : null,
		initialize : function() {
			var self = this;
			self.init();
		},
		createMap : function() {
			var self = this;
			function initialize() {
				try {
					var myLatlng = new google.maps.LatLng(45.9828612, 9.2635488);
					var mapOptions = {
						zoom : 16,
						center : myLatlng,
						mapTypeId : google.maps.MapTypeId.ROADMAP,
						mapTypeControl: false,
						panControl : false,
						zoomControl : true,
						zoomControlOptions : {
							style : google.maps.ZoomControlStyle.LARGE
						},
						scrollwheel : false,
						scaleControl : true,
						streetViewControl : true,
						draggable: false
					};
					self.map = new google.maps.Map(self.mapCanvas, mapOptions);
					var styles = [{
						stylers : [{
							saturation : 0
						}]
					}, {
						featureType : "landscape.man_made",
						stylers : [{
							lightness : 0
						}]
					}, {
						featureType : "landscape.natural",
						stylers : [{
							visibility : "simplified"
						}, {
							color : "#FFFFFF"
						}]
					}, {
						featureType : "road.highway",
						elementType : "geometry",
						stylers : [{
							color : "#ffc283"
						}, {
							saturation : 0
						}]
					}, {
						featureType : "road.arterial",
						elementType : "geometry",
						stylers : [{
							visibility : "simplified"
						}, {
							saturation : 100
						}, {
							color : "#ffa3b2",
							saturation : 10
						}]
					}
					
					];
					
					styles.push({
						featureType : "poi",
						stylers : [{
							visibility : "off"
						}]
					}, {
						featureType : "transit",
						stylers : [{
							visibility : "off"
						}]
					});

					self.map.setOptions({
						styles : styles
					});
					var contentString = '<div class="gm-content"><div class="gm-title">MyBellagioHome</div><div class="gm-text">via Visconti, 7</div></div>';
			
					var infowindow = new google.maps.InfoWindow({
						content : contentString
					});
			
					var marker = new google.maps.Marker({
						position : myLatlng,
						map : self.map,
						title : 'Wintex'
					});
					infowindow.open(self.map, marker);
					google.maps.event.addListener(marker, 'click', function() {
						infowindow.open(self.map, marker);
					});


				} catch(e) {
				}

			}

			initialize();
		},
		init : function() {
			var self = this;
			self.mapCanvas = self.$('#map').get(0);
			self.createMap();
			self.$('form').validate({
				submitHandler: function(form) {
					var dataArr = $(form).serializeArray();
				    var dataObj = _(dataArr).reduce(function(acc, field) {
				      acc[field.name] = field.value;
				      return acc;
				    }, {});
					var data = $.param(dataObj);
					var FID = dataObj.form_id.replace('webform_client_form_', '');
					$.ajax({
						data: $(form).serialize(),
						url: App.BASE_URL+'/webform_ajax/'+FID,
						type: 'POST',
						success: function(result) {
							/*var popup = new PopupView({
								classes: 'popup-thanks',
								content: $('<div class="content thanks-message font-light">'+result.message+'</div>'),
								useScroll: false
							});
							popup.init();
							$('body').append(popup.render().$el);
							popup.centerWrapper();
							$('.popup-thanks').lvClipMask();
							$('.popup-thanks').lvClipMask('open');
							popup.resize();
							popup.on('close', function() {
								$('.popup-thanks').lvClipMask('close', {
									onComplete: function() {
										popup.destroy();
										self.$('form')[0].reset();
									}
								});
							});
							*/
						}
					});
					return false;
				}
			});

			
			self.resize();
		},
		render : function() {

			return this;
		},
		resize : function() {
			var self = this;
			var $w = $(window);
			self.$('#map').css({
				height: $w.height()*1
			});
		}
	});

	return View;
});
