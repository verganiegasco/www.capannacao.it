/**
 * @author luigi
 *
 *
 */
require.config({
	paths : {
		"jquery" : "libs/jquery.min",
		"underscore" : "libs/underscore.min",
		"backbone" : "libs/backbone.min",
		"async" : 'libs/requirejs/plugins/async',
		"Modernizr" : "libs/modernizr",
		"text" : "libs/requirejs/plugins/text",
		"Swiper" : "libs/swiper/idangerous.swiper.min",
		"Swiper3dFlow" : "libs/swiper/plugins/3dflow/idangerous.swiper.3dflow.min",
		"TweenMax" : "libs/gsap/TweenMax.min",
		"CSSPlugin" : "libs/gsap/plugins/CSSPlugin.min",
		"BezierPlugin" : "libs/gsap/plugins/BezierPlugin.min",
		"ScrollToPlugin" : "libs/gsap/plugins/ScrollToPlugin.min",
		"jquery.browser" : "libs/jquery.browser.min",
		"jquery.fittext" : "libs/jquery.fittext",
		"jquery.flexslider" : "libs/flexslider/jquery.flexslider-min",
		"jquery.inview" : "libs/jquery.inview.min",
		"jquery.lettering" : "libs/jquery.lettering",
		"jquery.mCustomScrollbar" : "libs/mcustomscrollbar/jquery.mCustomScrollbar",
		"jquery.mousewheel" : "libs/jquery.mousewheel",
		"jquery.validate" : "libs/jquery.validate",
		"jquery.fancybox" : "libs/fancybox/jquery.fancybox",
		"jquery.supersized" : "libs/supersized/js/supersized.3.2.7.min",
		"jquery.tooltipster" : "libs/tooltipster/js/jquery.tooltipster.min",
		"jquery.waypoints" : "libs/waypoints/waypoints.min",
		"jquery.waypointssticky" : "libs/waypoints/shortcuts/sticky-elements/waypoints-sticky.min",
		"jquery.lv3DRotator" : "lv/jquery/btns/jquery.lv3drotator",
		"jquery.lvBtnArrow" : "lv/jquery/jquery.lvbtnarrow",
		"jquery.lvBtnColor" : "lv/jquery/jquery.lvbtncolor",
		"jquery.lvBtnRoundBorder" : "lv/jquery/btns/jquery.lvbtnroundborder",
		"jquery.lvCss" : "lv/jquery/jquery.lvcss",
		"jquery.lvGrid" : "lv/jquery/jquery.lvgrid",
		"jquery.lvGridAbsolute" : "lv/jquery/jquery.lvgridabsolute",
		"jquery.lvImgSlices" : "lv/jquery/jquery.lvimgslices",
		"jquery.lvLines" : "lv/jquery/jquery.lvlines",
		"jquery.lvMinHeight" : "lv/jquery/jquery.lvminheight",
		"jquery.lvPosition" : "lv/jquery/jquery.lvposition",
		"jquery.lvResize" : "lv/jquery/jquery.lvresize",
		"jquery.lvUnfold" : "lv/jquery/jquery.lvunfold",
		"lv.imagepreloadadapter" : "lv/preloader/lv.imagepreloadadapter",
		"lv.size" : "lv/lv.size",
		"lv.position" : "lv/lv.position",
		"lv.preloaderadapter" : "lv/preloader/lv.preloaderadapter",
		"lv.pxloaderadapter" : "lv/preloader/lv.pxloaderadapter",
		"PxLoader" : "libs/pxloader/PxLoader",
		"PxLoaderImage" : "libs/pxloader/PxLoaderImage"
	},
	shim : {
		"underscore" : {
			exports : "_"
		},
		"backbone" : {
			"deps" : ["underscore", "jquery"],
			"exports" : "Backbone" //attaches "Backbone" to the window object
		},
		"CSSPlugin" : {
			"deps" : ["TweenMax"]
		},
		"BezierPlugin" : {
			"deps" : ["TweenMax"]
		},
		"Swiper" : {
			"deps" : ["jquery"],
			"exports" : "Swiper"
		},
		"Swiper3dFlow" : {
			"deps" : ["jquery", "Swiper"]
		},
		"jquery.mCustomScrollbar":["jquery", "jquery.mousewheel"],
		"PxLoader" : {
			"exports" : "PxLoader"
		},
		"PxLoaderImage" : {
			"exports" : "PxLoaderImage",
			"deps": ["PxLoader"]
		},
		"jquery.fancybox" : ["jquery"],
		"jquery.browser" : ["jquery"],
		"jquery.lettering" : ["jquery"],
		"jquery.fittext" : ["jquery"],
		"jquery.flexslider" : ["jquery"],
		"jquery.mousewheel" : ["jquery"],
		"jquery.inview" : ["jquery"],
		"jquery.validate" : ["jquery"],
		"jquery.supersized" : ["jquery"],
		"jquery.tooltipster" : ["jquery"],
		"jquery.waypoints" : ["jquery"],
		"jquery.waypointssticky" : ["jquery", "jquery.waypoints"],
		"jquery.lv3DRotator": ["jquery", "TweenMax"],
		"jquery.lvPosition" : ["jquery", "lv.position"],
		"jquery.lvMinHeight" : ["jquery"],
		"jquery.lvResize" : ["jquery", "lv.size"],
		"jquery.lvCss" : ["jquery"],
		"jquery.lvGrid" : ["jquery", "jquery.lvCss"],
		"jquery.lvGridAbsolute" : ["jquery", "jquery.lvCss"],
		"jquery.lvLines" : ["jquery", "TweenMax"],
		"jquery.lvImgSlices" : ["jquery"],
		"jquery.lvUnfold" : ["jquery"],
		"jquery.lvBtnArrow" : ["jquery", "TweenMax"],
		"jquery.lvBtnColor" : ["jquery", "jquery.lettering", "TweenMax"],
		"jquery.lvBtnRoundBorder" : ["jquery", "TweenMax"],
		"lv.preloaderadapter" : {
			"exports" : "LV.PreloaderAdapter"
		},
		"lv.pxloaderadapter" : {
			"exports" : "LV.PxLoaderAdapter",
			"deps": ["PxLoader", "PxLoaderImage", "lv.preloaderadapter"]
		},
		"lv.imagepreloadadapter" : {
			"exports" : "LV.ImagePreloadAdapter",
			"deps": ["lv.preloaderadapter"]
		},
		"jquery.psBtnCloseX" : ["jquery", "jquery.lvBtnArrow", "jquery.lvLines"],
		"jquery.psBtnBordered" : ["jquery", "jquery.lvBtnColor", "jquery.lvLines", "jquery.lvCss"],
		"jquery.psBoxNews" : ["jquery", "jquery.lvBtnColor", "jquery.lvLines", "jquery.lvCss"],
		"jquery.psBtnRoundClip" : ["jquery", "jquery.lv3DRotator", "jquery.lvBtnRoundBorder"]
	}

});

if(typeof(console) === 'undefined') {
	window.console = {log: function() {} };
}

require(["jquery",
		"underscore",
		"backbone",
		"app/navigation/views/navigation.view",
		"app/navigation/views/navigationxs.view",
		"jquery.fancybox",
		"jquery.browser",
		"jquery.inview",
		"jquery.flexslider",
		"jquery.validate",
		"jquery.tooltipster",
		"jquery.lvPosition",
		"jquery.lvResize",
		"jquery.lvCss",
		"TweenMax",
		"CSSPlugin",
		"BezierPlugin",
		"ScrollToPlugin",
		"Modernizr",
		"PxLoader",
		"PxLoaderImage",
		"lv.preloaderadapter",
		"lv.pxloaderadapter",
		"lv.imagepreloadadapter"
		],
		function($, _, Backbone, NavigationView, NavigationXSView) {



	$('*[data-nav-step]').click(function() {
		var $t = $(this);
		var stepIndex = $t.parents('.js-nav-step').index();
		stepIndex = stepIndex>0? stepIndex-1:0;
		destIndex = $t.attr('data-nav-step') == 'next' ? stepIndex + 1 : stepIndex - 1;
		console.log(destIndex);
		var dest = $($('.js-nav-step').get(destIndex)).offset().top - $('.main-nav').height();
		TweenMax.to($(window), 1, {
			scrollTo : dest,
			ease : Circ.easeInOut
		});
	});
	$('*[data-scroll-to]').click(function() {
		var $t = $(this);
		var dest = $('*[data-anchor="' + $t.attr('data-scroll-to') + '"]').offset().top - $('.main-nav').height();
		TweenMax.to($(window), 1, {
			scrollTo : dest,
			ease : Circ.easeInOut
		});
	});
	$('.main-nav li a').click(function() {
		var $t = $(this);
		var dest = $('*[id="' + $t.attr('id') + '-container"]').offset().top - $('#main-header-wrapper').height();
		TweenMax.to($(window), 1, {
			scrollTo : dest,
			ease : Circ.easeInOut
		});
		return false;
	});


	$('.jqFancybox').fancybox({
		padding : 0,
		nextEffect : 'fade',
		prevEffect : 'none'
	});

	$('.jqTooltip').tooltipster({
		theme: 'tooltipster-mybellagiohome'
	});

  $('.js-full-flexslider').flexslider({
    percHeight: 1,
    animateScale: true
  });

	var view;
	var navigationXSView = new NavigationXSView();
	var navigationView = new NavigationView();
	switch(App.PAGE_ID) {
		case 'home':
		require(['app/copertina/main'], function(Main) {
			showContent();
		});
		break;
		default:
		showContent();
		break;
	}

	function showContent() {
		$('#main-wrapper').css({
			opacity: 0,
			display: 'block'
		});
		$('.general-preloader').remove();
		$(window).trigger('resize');
		TweenMax.to($('#main-wrapper'), .5, {
			opacity: 1
		});
	}





});
